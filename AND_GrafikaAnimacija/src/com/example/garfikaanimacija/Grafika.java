package com.example.garfikaanimacija;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class Grafika extends Activity {
	int currX = 0;
	int currY = 0;
	int dx = 1;
	int dy = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_grafika);

		final Slikica igralec = (Slikica) findViewById(R.id.slikica1);

		Timer t = new Timer();
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				currX = currX + dx;
				currY = currY + dy;
				igralec.setPosition(currX, currY);
				igralec.postInvalidate();
				if (currX > 300) dx = -1;
				if (currY > 400) dy = -1;
				if (currX < 1) dx = 1;
				if (currY < 1) dy = 1;
			}
		};
		t.schedule(task, 0, 10);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.grafika, menu);
		return true;
	}

}
