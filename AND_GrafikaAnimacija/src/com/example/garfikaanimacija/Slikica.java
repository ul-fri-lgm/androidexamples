package com.example.garfikaanimacija;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

public class Slikica extends View{
	Bitmap slika;
	Paint mPaint = new Paint();
	
	int posX=0, posY=0, width=100, height=100;

	public Slikica(Context context) {
		super(context);
		init(context);
	}

	public Slikica (Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	private void init(Context context) {
		slika = BitmapFactory.decodeResource(context.getResources(), R.drawable.igralec);
	}
	
	public void setPosition(int x, int y) {
		posX = x;
		posY = y;
	}
	
	public int getPosX() {
		return posX;
	}
	
	public int getPosY() {
		return posX;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		canvas.drawBitmap(slika, null, new Rect(posX, posY, posX+width, posY+height), mPaint);
	}
}
