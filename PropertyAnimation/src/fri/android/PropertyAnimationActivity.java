package fri.android;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

public class PropertyAnimationActivity extends Activity implements
		AnimationListener {

	View v;
	Boolean STOP = false;	

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		TextView tv = (TextView) findViewById(R.id.textview);
		tv.setPadding(600, 300, 0, 0);
		tv.setTextSize(50);
		Button izhod = (Button) findViewById(R.id.button1);
		izhod.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				System.exit(0);
				
			}
		});
	}

	public void startAnimation(View view) {
		Animation animation = AnimationUtils.loadAnimation(this,
				R.anim.animation);
		
		animation.setAnimationListener(this);
		View animatedView = findViewById(R.id.textview);
		animatedView.startAnimation(animation);
	}

	public void stopAnimation(View view) {
		//Toast.makeText(this, "Animacija se bo kmalu zaključila.", Toast.LENGTH_SHORT).show();
		STOP = true;
	}

	@Override
	public void onAnimationStart(Animation animation) {
		//Toast.makeText(this, "Pognal si animacijo", Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		if (STOP == false)
			startAnimation(v);
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		//Toast.makeText(this, "Animation rep", Toast.LENGTH_SHORT).show();
	}
}