package fri.android;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.View.OnCreateContextMenuListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;

public class AND_InterakcijaActivity extends Activity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        

        View v = findViewById(R.id.tv1);
        
        v.setFocusable(true);
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        
        OnClickListener clickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.d("INTERAKCIJA","KLIK!");
				
			}
		};
        
        OnKeyListener keyListener = new OnKeyListener() {
			
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				int action=event.getAction();
				switch (action) {
					case KeyEvent.ACTION_DOWN:
						Log.d("INTERAKCIJA","Tipka dol - koda: " + event.getKeyCode());
						break;
					case KeyEvent.ACTION_UP:
						Log.d("INTERAKCIJA","Tipka gor - koda: " + event.getKeyCode());
						break;
				}
				return true; //ne posredujemo tipke naprej
			}
		};
        OnLongClickListener longListener = new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				Log.d("INTERAKCIJA","LONG KLIK!");
				return true; //ne posredujemo long toucha naprej (vendar potem ne dobimo menuja)
			}
		};
        OnTouchListener touchListener = new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int action=event.getAction();
				switch (action) {
					case MotionEvent.ACTION_DOWN:
						Log.d("INTERAKCIJA","Prst dol na "+event.getX()+ ", "+event.getY());
						break;
					case MotionEvent.ACTION_UP:
						Log.d("INTERAKCIJA","Prst gor na "+event.getX()+ ", "+event.getY());
						break;
					case MotionEvent.ACTION_MOVE:
						Log.d("INTERAKCIJA","Prst premik na "+event.getX()+ ", "+event.getY());
						break;
				}
				
				return false; //da lahko dobimo click, longTouch, ... dela
				//ce vrnemo true - ne poslejmo toucha naprej, vendar potem ne dela click, longTouch in menuCreate
			}
		};
        OnCreateContextMenuListener createMenuListner = new OnCreateContextMenuListener() {
			
			@Override
			public void onCreateContextMenu(ContextMenu menu, View v,
					ContextMenuInfo menuInfo) {
				Log.d("INTERAKCIJA","MENU!");
				
			}
		};
        
        v.setOnClickListener(clickListener);
        v.setOnKeyListener(keyListener);
        v.setOnLongClickListener(longListener);
        v.setOnTouchListener(touchListener);
        v.setOnCreateContextMenuListener(createMenuListner);
    }
    
    
}