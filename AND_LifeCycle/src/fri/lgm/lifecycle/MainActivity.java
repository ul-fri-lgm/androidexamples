package fri.lgm.lifecycle;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle stanje) {
        super.onCreate(stanje);
		Log.d("LIFE","create");
		if (stanje!=null) {
			int a=stanje.getInt("a");
			Log.d("LIFE","Vrednost: "+a);
		} else {
			Log.d("LIFE","Brez vrednosti");
		}
        setContentView(R.layout.activity_main);
    }
    
    public void onStart() {
    		super.onStart();
		Log.d("LIFE","start");
    	
    }
    
    public void onResume() {
    		super.onResume();
		Log.d("LIFE","resume");
    	
    }
    
    public void onPause() {
    		super.onPause();
		Log.d("LIFE","pause");
    	
    }
    
    public void onStop() {
    		super.onStop();
		Log.d("LIFE","stop");
    	
    }
    
    public void onDestroy() {
    		super.onDestroy();
    		Log.d("LIFE","destroy");
    }
    
    public void onSaveInstanceState(Bundle stanje) {
    		stanje.putInt("a",10);
		Log.d("LIFE","hrani stanje");
    }

    
}
