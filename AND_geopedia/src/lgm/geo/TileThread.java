package lgm.geo;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.LinkedList;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Debug;
import android.util.SparseArray;

public class TileThread {
	
	private boolean isRunning;
	private Thread runningThread;

	private boolean stopThread;
	private LinkedList<Tile> requestQueue;
	private TileManager tileManager;
	private LinkedList<Tile> memoryList;
    private SparseArray<Bitmap> bitmapCache; //holds bitmap of max zoom level
	
	//called for UI thread
	public TileThread() {
		if (Util.DEBUG) Util.logd("TT constructor");
		requestQueue=new LinkedList<Tile>();
		memoryList=new LinkedList<Tile>();
		bitmapCache=new SparseArray<Bitmap>();
		isRunning=false;
		runningThread=null;
	}

	//called for UI thread
	public void setCompletedCallback(TileManager manager) {
		if (Util.DEBUG) Util.logd("TT setting callback");
		this.tileManager=manager;
	}

	//called for UI thread
	public void start() {
		if (Util.DEBUG) Util.logd("TT starting");
		if (isRunning) return;
		runningThread=new Thread(new Runnable() {
			public void run() {
				TileThread.this.run();
			}
		});
		runningThread.start();
		isRunning=true;
	}

	//called for UI thread
	//Tile t - tile to load
	//Rect r - boundaries of view in global space
	public void schedule(Tile t) {
		if (Util.DEBUG) Util.logd("TT add new tile to reqQ");
		if (t==null) return;
		if (!isRunning) return;
		synchronized (requestQueue) {	
			requestQueue.addFirst(t);
		}
		//wakeUp();
	}   

	//called for UI thread
	public void clear() {
		if (Util.DEBUG) Util.logd("TT clear reqQ");
	    synchronized(requestQueue) {
	    	requestQueue.clear();
	    }
	}

	//called for UI thread
	public void destroy() {
		if (Util.DEBUG) Util.logd("TT destroying");
		if (!isRunning) return;
		synchronized (runningThread) {
			if (Util.DEBUG) Util.logd("TT stopping");
			stopThread=true;
			wakeUp();
		}
		try {
			if (Util.DEBUG) Util.logd("TT waiting to join");
			runningThread.join();
			if (Util.DEBUG) Util.logd("TT end");
		} catch (Exception e) {
			if (Util.DEBUG) Util.logd("Thread stop error %s",e.getMessage());
		}
		freeMemory(true); //free all memory!
	}

	private void wakeUp() {
		if (!isRunning) return;
		if (Util.DEBUG) Util.logd("TT waking up");
		synchronized (runningThread) {
			runningThread.interrupt();
		}
	}
	
	private void run() {
		if (Util.DEBUG) Util.logd("TT run iteration started");
	    try {
            while (!stopThread) {
            	runIteration();
            }
        } catch (Exception e) {
        	Util.logd("Run-Loop Exception, stopping thread -%s",e.getMessage());
        	StringWriter sw=new StringWriter();
        	e.printStackTrace(new PrintWriter(sw));
        	Util.logd(sw.toString());
	    }
        if (Util.DEBUG) Util.logd("TT run iteration stopped");
	}

    private void runIteration() {
		Tile t=null;
    	synchronized (requestQueue) {
    		t=requestQueue.poll(); //take first element from queue
		}
    	if (t!=null && !t.isLoaded() && tileManager!=null) {
    		final Rect manager_r=tileManager.getGlobalRect();
    		final Rect tile_r=t.getGlobalRect();
    		//find if insects with tile t rect
    		final int level=tileManager.getZoomLevel();
    		if (Rect.intersects(tile_r,manager_r) && level==t.getLevel()) {
    			if (level>TileManager.MAX_ZOOM) {
    				loadFromUpper(t);
    			} else {
    				loadFromFile(t);
    			}
    	    	Debug.MemoryInfo minfo=new Debug.MemoryInfo();
    	    	Debug.getMemoryInfo(minfo);
    	    	Util.logd("Memory usage: %d KB",minfo.otherPrivateDirty);
    		}
    	}
    	try {
			//Util.logd("TT waiting");
    		synchronized (this) {
    			wait(50); //in ms
			}
		} catch (InterruptedException e) {
			// pass
		}
	}
    
    private void loadFromFile(Tile t) {
		int key=t.getKey();
		int level=t.getLevel();
    	Util.logd("TT file tile [%d,%d]",t.getI(),t.getJ());
		for (int i=0;i<2;i++) {
			boolean found=false;;
			int res=0;
			if (level==TileManager.MAX_ZOOM) {
				//try to find in bitmap cache
				Bitmap bmp=bitmapCache.get(key);
				if (bmp!=null) {
					Util.logd("Found in cache");
					t.setBitmap(bmp);
					found=true;
				}
			}
			if (!found) res=t.load();  //try to load
			if (res==0) { //no problems while loading image
            	Util.logd("TT file tile [%d,%d] loaded",t.getI(),t.getJ());
				if (Util.DEBUG) Util.logd("TT tile loaded");
				tileManager.onTileLoaded(t);
				//set to the end of memoryList
                if (t.isInMemory()) memoryList.remove(t);
                memoryList.add(t);
                t.setInMemory(true);
                if (!found && level==TileManager.MAX_ZOOM) {
                	//add to cache
                	bitmapCache.put(t.getKey(),t.getBitmap());
                }
				break; //do not try again
			}
			if (res==1) { //this image failed to load (file missing/corrupted??)
				if (Util.DEBUG) Util.logd("TT tile load error");
				tileManager.onTileLoaded(t); //render as not loaded!
				break; //do not try again
			}
			if (res==2) {
	    		Util.logd("FILE: Out of memory (size: %d)",memoryList.size());
				if (Util.DEBUG) Util.logd("TT no memory");
				//empty memory
				freeMemory(false);
				continue; //try again
			}
		}
    }
    
    private void loadFromUpper(Tile t) {
    	int tile_i=t.getI();
    	int tile_j=t.getJ();
    	Util.logd("TT Loading scaled tile [%d,%d]",tile_i,tile_j);
    	//find corresponding tile(i,j) on TileManager.MAX_ZOOM level
    	int upper_i=tile_i;
    	int upper_j=tile_j;
    	int l=t.getLevel();
    	int size=Tile.SIZE;
    	int x=0;
    	int y=0;
    	while (l>TileManager.MAX_ZOOM) {
    		size/=2;
    		x=(x/2)+(upper_i%2==0?0:size);
    		y=(y/2)+(upper_j%2==0?0:size);
    		upper_i/=2;
    		upper_j/=2;
    		l--;
    	}
    	int yy=Tile.SIZE-(size+y);

		Util.logd("TT scaled tile [%d,%d] <- [%d,%d]",tile_i,tile_j,upper_i,upper_j);
    	//check if in bitmapCache
    	int key=Tile.computeKey(upper_i,upper_j,TileManager.MAX_ZOOM);
    	Bitmap bmp=bitmapCache.get(key);
		for (int k=0;k<3;k++) { 
	    	if (bmp==null) {
	    		Util.logd("TT [%d,%d] not found",upper_i,upper_j);
	    		loadFromFile(new Tile(upper_i,upper_j,TileManager.MAX_ZOOM));
	    	}
	    	bmp=bitmapCache.get(key);
			try {
				Util.logd("getting clip [%d,%d]",upper_i,upper_j);
				Bitmap tmp = Bitmap.createBitmap(bmp,x,yy,size,size);
				Util.logd("creating scaled [%d,%d]",upper_i,upper_j);
				Bitmap tmp2 = Bitmap.createScaledBitmap(tmp,Tile.SIZE,Tile.SIZE,true);
				Util.logd("done scaling [%d,%d]",tile_i,tile_j);
				t.setBitmap(tmp2);
				tileManager.onTileLoaded(t); //render as not loaded!
				if (t.isInMemory()) memoryList.remove(t);
				memoryList.add(t);
				t.setInMemory(true);
				return;
	    	} catch (OutOfMemoryError e) {
	    		Util.logd("UPPER: Out of memory (size: %d)",memoryList.size());
	    		//failed to create bitmap - free memory and try again
	    		freeMemory(false);
	    	}
		}
		//load failed - assign null bitmap
		t.setBitmap(null);
    }
    
    private void freeMemory(boolean all) {
    	if (Util.DEBUG) Util.logd("Freeing - num tiles: %d",memoryList.size());
    	int n=memoryList.size();
    	int nn=n/4;
    	if (nn<2) nn=2;
    	if (all) nn=n;
    	//always free oldest 20% of tiles
    	
    	for (int i=0;i<nn;i++) {
    		Tile t = memoryList.poll();
    		if (t == null) break;
            t.setInMemory(false);
            Bitmap b=t.reclaimBitmap();
            if (b!=null && !b.isRecycled()) {
            	Util.logd("Freeing tile [%d,%d]",t.getI(),t.getJ()); 
            	b.recycle();
            }
        	//delete from cache as well
        	if (t.getLevel()==TileManager.MAX_ZOOM) {
        		Util.logd("Deleting from cache");
        		bitmapCache.delete(t.getKey());
        	}
        }
    	//call garbage collector
    	//System.gc();
    }
}

