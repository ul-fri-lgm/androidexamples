package lgm.geo;

import android.graphics.Rect;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ZoomControls;

public class TileManager {

	private final static int INITIAL_TILE_CACHE_SIZE=20;
	public final static int MAX_ZOOM=15;
	private final static int MIN_ZOOM=9;

    //current tiles (on borders of the view)
    private int[] curr_tiles_coor=new int[4];

    //zoom
    private ZoomControls zoomer;
    
    
    private TileView tileView;
    private TileThread tileThread;
        
    //all tiles
    private Tile[] visibleTiles;
    private SparseArray<Tile> tileCache;
    
    //view parameters 
    //view manager offset from coordinate 0,0 in global pixel space
    //pixel space changes on zoom in/out!!!
    //offset - point (bottom,left) on view in global pixel space
    private int zoom_level;
    private int view_width;
    private int view_height;
    private int center_x;
    private int center_y;
    //private Point globalCoor;
    private Rect globalRect;
    
    //location update (gps)
    private boolean inTrackMode;
    

	public TileManager() {
		if (Util.DEBUG) Util.logd("TM constructor");
		tileCache=new SparseArray<Tile>(INITIAL_TILE_CACHE_SIZE);
		tileThread=new TileThread();
		tileThread.setCompletedCallback(this);
		tileThread.clear();
		tileThread.start();
	}
	
	public void initState(Bundle state) {
		if (Util.DEBUG) Util.logd("TM resetState");
		zoom_level=15;
		center_x=20000; //11450;
		center_y=26000; //14700;
		inTrackMode=true;
		if (state!=null) {
			zoom_level=state.getInt("zoom_level",zoom_level);
			center_x=state.getInt("center_x",center_x);
			center_y=state.getInt("center_y",center_y);
			inTrackMode=state.getBoolean("inTrackMode",inTrackMode);
		}
		int w2=view_width/2;
		int h2=view_height/2;
		globalRect=new Rect(center_x-w2,center_y-w2,center_x-w2+view_width,center_y-h2+view_height);
		//updateAll(false);
        //invalidateView();
	}
	
	public void saveState(Bundle state) {
		if (state==null) return;
		state.putInt("zoom_level",zoom_level);
		state.putInt("center_x",center_x);
		state.putInt("center_y",center_y);
		state.putBoolean("inTrackMode",inTrackMode);
	}

    public Rect getGlobalRect() {
		return globalRect;
	}
    
    public int getZoomLevel() {
    	return zoom_level;
    }
	
    // Runs from the UI thread - when view size is changed
    public void onSizeChanged(int w, int h) {
    	if (Util.DEBUG) Util.logd("TM onSizeChanged");
        //logd("onSizeChanged: %dx%d", w,h);
        view_width=w;
        view_height=h;
        globalRect.left=center_x-w/2;
        globalRect.top=center_y-h/2;
        globalRect.right=globalRect.left+w;
        globalRect.bottom=globalRect.top+h;
        updateAll(true);
        invalidateView();
    }
    
    //called from onOffset, onSizeChanged, onZoomChanged
    private void updateAll(boolean force) {
    	if (Util.DEBUG) Util.logd("TM updateAll");
        final int tile_size = Tile.SIZE;

        final int nx = (view_width/tile_size) + 2;
        final int ny = (view_height/tile_size) + 2;
        final int nn = nx * ny;
        if (visibleTiles == null || visibleTiles.length != nn) {
        	visibleTiles = new Tile[nn];
            force = true;
        }

        //calculate new border tiles that will be visible on screen
        final Rect rect=globalRect;
        final int[] new_tiles_coor=new int[4];
        new_tiles_coor[0]=rect.left/tile_size;
        new_tiles_coor[1]=rect.top/tile_size;
        //must sub with 1 
        //exp: scr width=320, start left=192 -> right=512 
        //-> visible tiles are 0 and 1, but without -1 you get 2
        new_tiles_coor[2]=(rect.right-1)/tile_size; 
        //the same reason as above
        new_tiles_coor[3]=(rect.bottom-1)/tile_size;
        
        //compare with existing tiles on screen
        boolean same_border_tiles=true;
        for (int ii=0;ii<4;ii++) {
        	if (curr_tiles_coor[ii]!=new_tiles_coor[ii]) {
        		same_border_tiles=false;
        		break;
        	}
        }
        

        if (!force && same_border_tiles) return; //all border tiles are the same
        
        //new center tile
        curr_tiles_coor=new_tiles_coor; //save new state
        
        int k = 0;
        //only request tiles that are visible
        for (int ii = new_tiles_coor[0]; ii<=new_tiles_coor[2]; ii++) {
        	for (int jj = new_tiles_coor[1]; jj<=new_tiles_coor[3]; jj++,k++) {
                visibleTiles[k] = requestTile(ii,jj,zoom_level);
            }
        }
        //invisible tiles should be set to null
        for (; k < nn; k++) {
        	visibleTiles[k] = null;
        }
    }
    
    // Runs from the UI (activity) thread
    public void setTileView(TileView view) {
    	if (Util.DEBUG) Util.logd("TM setView");
        tileView = view;
    }
    
    public void setTrackMode(boolean mode) {
    	if (Util.DEBUG) Util.logd("TM setTrackMode");
    	inTrackMode=mode;
    }
    
    // Runs from the UI thread
    public Tile[] getVisibleTiles() {
    	if (Util.DEBUG) Util.logd("TM getVisibleTiles");
        return visibleTiles;
    }
    
    // Runs from the UI thread
    public void onOffset(int dx,int dy) {
    	if (Util.DEBUG) Util.logd("TM onOffset");
    	center_x+=dx;
    	center_y+=dy;
        globalRect.offset(dx,dy);
        updateAll(false);
        invalidateView();
    }
    
    
    
    public void onLocationChange(int cx,int cy) {
    	if (Util.DEBUG) Util.logd("TM onLocationChange");
    	if (inTrackMode) {
    		onOffset(cx-center_x,cy-center_y);
    	}
    }
        
    // Runs from the UI thread - updateAll
    private Tile requestTile(int i, int j, int level) {
    	/*f (Util.DEBUG)*/ Util.logd("TM request tile %d,%d",i,j);
    	
        Tile t = null;
        int key = Tile.computeKey(i,j,level);
        t = tileCache.get(key);
        if (t == null) {
        	if (Util.DEBUG) Util.logd("TM create tile %d,%d",i,j);
        	t = new Tile(i,j,level);
            tileCache.put(key,t);
        }
        if (!t.isLoaded()) {
        	if (Util.DEBUG) Util.logd("TM schedule tile %d,%d",i,j);
        	if (tileThread!=null) {
        		Util.logd("TM schedule tile %d,%d",i,j);
        		tileThread.schedule(t);
        	} else {
        		if (Util.DEBUG) Util.logd("TM tileThread is null?!");
        	}
        }
        return t;
    }
    
    public void onZoomChange(int step) {
    	if (Util.DEBUG) Util.logd("TM onZoomChange");
    	int zoom=(step>0)?(zoom_level+1):(zoom_level-1);
    	//if (zoom>MAX_ZOOM) zoom=MAX_ZOOM;
    	if (zoom>(MAX_ZOOM+1)) zoom=MAX_ZOOM+1;
    	if (zoom<MIN_ZOOM) zoom=MIN_ZOOM;
    	if (zoom==zoom_level) return; //no zoom change
    	//calc new center
    	if (zoom>zoom_level) {
    		center_x=2*center_x;
    		center_y=2*center_y;
    	} else {
    		center_x=center_x/2;
    		center_y=center_y/2;
    	}
    	//calc new rect
    	final int w2=view_width/2;
    	final int h2=view_height/2;
        globalRect.left=center_x-w2;
        globalRect.top=center_y-h2;
        globalRect.right=globalRect.left+view_width;
        globalRect.bottom=globalRect.top+view_height;
    	zoom_level=zoom;
        updateAll(true);
        invalidateView();    	
    }

    //called from tileThread
	public void onTileLoaded(Tile t) {
		if (Util.DEBUG) Util.logd("TM tile loaded");
		invalidateTile(t);
	}
    
    private void invalidateView() {
    	if (Util.DEBUG) Util.logd("TM invalidateView");
        if (tileView != null) {
            tileView.postInvalidate();
        }
    }
	
    private void invalidateTile(Tile tile) {
        if (tile == null) return;
        if (tileView == null) return;
        Rect r=new Rect();//getTileScreenRect(tile);
        if (isTileVisible(tile,r,null) && tile.getLevel()==zoom_level) {
        	if (Util.DEBUG) Util.logd("Invalidate (%d,%d - %d,%d)",r.left,r.top,r.right,r.bottom);
        	tileView.postInvalidate(r.left,r.top,r.right,r.bottom);
        }
    }
    
    public boolean isTileVisible(Tile t,Rect screen_rect,Rect tile_rect) {
    	Rect m_rect=globalRect;
    	Rect t_rect=t.getGlobalRect();
    	//Util.logd("Manager: %s",m_rect.toShortString());
    	//Util.logd("Screen: %s",screen_rect.toShortString());
    	final int tile_size=Tile.SIZE;
    	if (!Rect.intersects(globalRect,t_rect)) return false;
    	
    	//calculate screen & tile rect
    	Rect intersection=new Rect(t_rect);
    	intersection.intersect(m_rect);
    	if (screen_rect!=null) {
    		screen_rect.left=intersection.left-m_rect.left;
    		screen_rect.right=intersection.right-m_rect.left;
    		screen_rect.top=view_height-(intersection.bottom-m_rect.top);
    		screen_rect.bottom=view_height-(intersection.top-m_rect.top);
    		//Util.logd("Screen: %s",screen_rect.toShortString());
    	}
    	if (tile_rect!=null) {
    		tile_rect.left=intersection.left-t_rect.left;
    		tile_rect.right=intersection.right-t_rect.left;
    		tile_rect.top=tile_size-(intersection.bottom-t_rect.top);
    		tile_rect.bottom=tile_size-(intersection.top-t_rect.top);
    		//Util.logd("TileS: %s",tile_rect.toShortString());
    	}
    	return true;
    }
    
    public void destroy() {
        if (tileThread != null) {
        	if (Util.DEBUG) Util.logd("TM kill TileThread");
            tileThread.destroy();
            tileThread = null;
        }
    }
    
    public void showZoom(boolean show) {
    	if (show && zoomer.getVisibility() != View.VISIBLE) {
			zoomer.show();
    	}
    	if (!show) {
    		zoomer.hide();
    	}
    }

	public void setZoomControl(ZoomControls zoom) {
		if (zoom!=null) {

            zoom.setOnZoomInClickListener(new OnClickListener() {
                public void onClick(View v) {
                	onZoomChange(1);
                }
            });

            zoom.setOnZoomOutClickListener(new OnClickListener() {
                public void onClick(View v) {
                	onZoomChange(-1);
                }
            });
			zoomer=zoom;
			showZoom(true);
		}
	}

}
