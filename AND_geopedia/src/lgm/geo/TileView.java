package lgm.geo;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

//Workflow:
//	 * - created from XML
//	 * - activity assigns TileManager upon creation (no size yet)
//	 * - onSizeChanged =>TileManager.onSizeChanged to recompute tiles
//	 * - trackball/mouse => change offset for panning


public class TileView extends View {

	private static final int DELTA = 50;
	
    public static Bitmap emptyTile;
    public static Bitmap noTile;
    
	private TileManager tileManager;
	private Paint redPaint;
	
	//drawing rect
    Rect tile_screen_rect=new Rect();
    Rect tile_rect=new Rect();
    Rect screen_bounds=new Rect();	
	
	float touch_start_x,touch_start_y; //remember start of touch event

	private PositionManager positionManager;
	
    static {
    	final int size=Tile.SIZE;
    	int[] buffer1=new int[size*size];
    	int[] buffer2=new int[size*size];
		for (int y=0;y<size;y++) {
			int yy=y*size;
			for (int x=0;x<size;x++) {
				buffer1[yy+x]=0xffffff;
				buffer2[yy+x]=0x00;
			}
		}
		TileView.emptyTile=Bitmap.createBitmap(buffer1,size,size,Bitmap.Config.RGB_565);
		TileView.noTile=Bitmap.createBitmap(buffer2,size,size,Bitmap.Config.RGB_565);
    }	
			
	public TileView(Context context, AttributeSet attrs) {
		super(context,attrs);
		if (Util.DEBUG) Util.logd("TW constructor");
		redPaint = new Paint();
		redPaint.setColor(0xFFFF0000);
		redPaint.setStyle(Paint.Style.STROKE);
	}
	
	public void setTileManager(TileManager tm) {
		if (Util.DEBUG) Util.logd("TW setTileManager");
		tileManager = tm;
    }

	public void setPositionManager(PositionManager pm) {
		if (Util.DEBUG) Util.logd("TW setPositionManager");
		positionManager = pm;
	}
	
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (Util.DEBUG) Util.logd("TW onSizeChanged");
        if (tileManager != null) tileManager.onSizeChanged(w,h);
    }
	

		
	@Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (tileManager==null) return;
        if (Util.DEBUG) Util.logd("TW onDraw");
        Tile[] tiles = tileManager.getVisibleTiles();
        if (tiles == null || tiles.length == 0) {
            // show empty screen
            return;
        }

        boolean useBounds=canvas.getClipBounds(screen_bounds);
        if (Util.DEBUG)  Util.logd("Draw screen: %s",screen_bounds.toShortString());
        for (Tile t : tiles) {
            if (t == null) continue;
            if (!tileManager.isTileVisible(t,tile_screen_rect,tile_rect)) continue;
            if (useBounds && !Rect.intersects(tile_screen_rect,screen_bounds)) continue;
            Bitmap bmp = t.getBitmap();
            if (bmp==null) {
            	if (!t.isLoaded()) {
            		bmp=TileView.emptyTile; //can still be loaded
            	} else {
            		bmp=TileView.noTile; //cannot be loaded
            	}
            } else {
            	Util.logd("Drawing on %s tile(%d,%d) %s",screen_bounds.toShortString(),t.getI(),t.getJ(),tile_screen_rect);	
            }
            canvas.drawBitmap(bmp,tile_rect,tile_screen_rect,null /*paint*/);
            //canvas.drawRect(tile_screen_rect, redPaint);
            
        }
        if (positionManager!=null) {
        	positionManager.drawLocation(canvas);
        }
    }
	
    private void offset(int dx,int dy) {
    	if (tileManager!=null) {
    		tileManager.setTrackMode(false);
    		tileManager.onOffset(dx,dy);
    	}
    }
        
    @Override
    public boolean  onKeyDown (int keyCode, KeyEvent event) {
    	switch (keyCode) {
    		case KeyEvent.KEYCODE_DPAD_DOWN:
    			offset(0,-DELTA);
    			return true;
    		case KeyEvent.KEYCODE_DPAD_UP:
    			offset(0,DELTA);
    			return true;
    		case KeyEvent.KEYCODE_DPAD_LEFT:
    			offset(-DELTA,0);
    			return true;
    		case KeyEvent.KEYCODE_DPAD_RIGHT:
    			offset(DELTA,0);
    			return true;
    		case KeyEvent.KEYCODE_DPAD_CENTER:
    			if (tileManager!=null) {
    				tileManager.setTrackMode(true);
    			}
    			return true;
    	}
    	return super.onKeyDown(keyCode,event);
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
        	case MotionEvent.ACTION_DOWN:
        		if (tileManager!=null) {
        			touch_start_x = event.getX();
        			touch_start_y = event.getY();
        			return true;
        		}
        		break;
        	case MotionEvent.ACTION_MOVE:
        		if (tileManager != null) {
        			float newx=event.getX();
        			float newy=event.getY();
        			int dx = -(int)(newx - touch_start_x);
        			int dy = (int)(newy - touch_start_y);
        			offset(dx,dy);
        			touch_start_x=newx;
        			touch_start_y=newy;
        			return true;
        		}
        		break;
        }
        return super.onTouchEvent(event);
    }
    


}
