package lgm.geo;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

public class PositionManager {
	
	//zoom factors for zoom level: 9,10,11,12,13,14,15 - 1pixel is how many meters? 
	private static final int GK_DX=-19004;
	private static final int GK_DY=368004;
	
	private Paint locPaint;
	private Location lastLocation;
	private TileManager tileManager;
	private LocationManager locationManager;
	private LocationListener locListener;
	
	public PositionManager(Context context) {
		
		locPaint=new Paint();
		locPaint.setColor(0x800000FF);
		locPaint.setStyle(Paint.Style.FILL);
        
		locationManager=(LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
		locListener=new LocationListener() {
	
			public void onLocationChanged(Location location) {
				locationChanged(location);
			}
			
			//unimplemented
			public void onProviderDisabled(String provider) {}
			public void onProviderEnabled(String provider) {}
			public void onStatusChanged(String provider, int status,Bundle extras) {}
		};
		registerListener();
	}
	
	public void registerListener() {
		if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,locListener);
		} else if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,locListener);
		}
	}
	

	public void initState(Bundle state) {
		if (Util.DEBUG) Util.logd("PM initState");
		lastLocation=null;
		if (state!=null) {
			double latitude=state.getDouble("latitude",0.0);
			double longitude=state.getDouble("longitude",0.0);
			long time=state.getLong("time",0);
			if (latitude!=0.0 && longitude!=0.0) {
				lastLocation=new Location("state");
				lastLocation.setLatitude(latitude);
				lastLocation.setLongitude(longitude);
				lastLocation.setTime(time);
			}
		}
	}
	
	public void saveState(Bundle state) {
		if (state==null) return;
		if (lastLocation==null) return;
		state.putDouble("latitude",lastLocation.getLatitude());
		state.putDouble("longitude",lastLocation.getLongitude());
		state.putLong("time",lastLocation.getTime());
	}
      
	public void setTileManager(TileManager manager) {
		tileManager=manager;
	}
	
	public void drawLocation(Canvas canvas) {
		if (lastLocation==null) return;
		if (tileManager==null) return;
		int zoom_level=tileManager.getZoomLevel();
		Point p=toPixelCoor(lastLocation,zoom_level);
		if (p==null) return;
		Rect m_rect=tileManager.getGlobalRect();
		if (!(p.x>=m_rect.left && p.x<m_rect.right && p.y>=m_rect.top && p.y<m_rect.bottom)) {
			//not visible
			return;
		}
		int center_x=p.x-m_rect.left;
		int center_y=m_rect.bottom-p.y;
		canvas.drawCircle(center_x,center_y,10,locPaint);
	}
    
	private void locationChanged(Location location) {
		lastLocation=location;
		if (tileManager!=null) {
			//calculate point
			int zoom_level=tileManager.getZoomLevel();
			Point p=toPixelCoor(location,zoom_level);
			if (p!=null) {
				tileManager.onLocationChange(p.x,p.y);
			}
		}
	}
    
   	private Point toPixelCoor(Location l, int zoom_level) {
    	if (l==null) return null;
    	Point p=new Point();
    	double[] xy_gk=new double[2];
    	double fi=l.getLatitude();
    	double lam=l.getLongitude();
		double[] fi_lam_gps={fi,lam};
		GpsUtil.gps2gk(fi_lam_gps,xy_gk);
		int factor=(int) Math.pow(2,17-zoom_level);
		//x,y change during transformation!!!
		p.x=(int) ((xy_gk[1]-GK_DY)/factor);
		p.y=(int) ((xy_gk[0]-GK_DX)/factor);
    	return p;
    }
	
}
