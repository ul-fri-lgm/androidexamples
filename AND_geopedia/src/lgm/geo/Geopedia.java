package lgm.geo;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Debug;
import android.os.PowerManager;
import android.widget.ZoomControls;

public class Geopedia extends Activity {
	
	private TileManager tileManager;
	private PositionManager positionManager;

    protected PowerManager.WakeLock mWakeLock; 
	
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Util.DEBUG) Util.logd("Geo onCreate");
        //set default view of this activity
        setContentView(R.layout.tile);
        
        //Debug.startMethodTracing("geopedia");
        TileView tileView = (TileView) findViewById(R.id.tile_view);
        //set view now
        tileView.setFocusable(true);
        tileView.setFocusableInTouchMode(true);
        tileView.requestFocus();
        
        ZoomControls zoomer = (ZoomControls) findViewById(R.id.zoom_control);

        
        
        tileManager = new TileManager();        
        tileManager.setTileView(tileView);
        tileManager.setZoomControl(zoomer);
        tileManager.initState(savedInstanceState);
        
        positionManager=new PositionManager(this);
        positionManager.setTileManager(tileManager);
        positionManager.initState(savedInstanceState);

        tileView.setTileManager(tileManager);
        tileView.setPositionManager(positionManager);

        /* This code together with the one in onDestroy()
         * will make the screen be always on until this Activity gets destroyed. */
        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK,"Geopedia");
        mWakeLock.acquire(); 
        
    }
    
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	if (Util.DEBUG) Util.logd("SavedInstance");
    	tileManager.saveState(outState);
    	positionManager.saveState(outState);
    }
    
    public void onDestroy() {
    	super.onDestroy();
    	if (Util.DEBUG) Util.logd("Geo onDestroy");
        mWakeLock.release(); 
    	tileManager.destroy(); //do not kill thread onStop!!!
    	//Debug.stopMethodTracing();
    	if (Util.DEBUG) Util.logd("Geo tileManager destory");
    }
}