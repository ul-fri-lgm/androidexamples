package lgm.geo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.os.Environment;

/**
 * A Map tile, for a given coordinates
 * Tiles are 256x256 pixels wide
 */
public class Tile {

    public final static int SIZE = 512; //2*256;
    private static final String geo_dir="/geopedia/"; 
    private static String dir=Environment.getExternalStorageDirectory().toString()+geo_dir;
    
    private Rect globalRect;
    private int level;
    private int i;
	private int j;
    private Bitmap bitmap;
    private boolean loaded;
    private boolean inMemory;
    
    public static int computeKey(int i,int j,int l) {
    	return l*100000+i*1000+j;
    }    
    
	public Tile(int ii,int jj,int l) {
        level=l;
        i=ii;
        j=jj;
        final int x=i*Tile.SIZE;
        final int y=j*Tile.SIZE;
        globalRect=new Rect(x,y,x+Tile.SIZE,y+Tile.SIZE);
        loaded=false;
        bitmap=null;
        inMemory=false;
        //load(); - load is expensive - must be called from non-UI thread
    }
    
    public int getLevel() {
    	return level;
    }
    
    public boolean isLoaded() {
    	return loaded;
    }
    
    public Rect getGlobalRect() {
    	return globalRect;
    }
    
    public Bitmap getBitmap() {
    	return bitmap;
    }
    
    public int getI() {
		return i;
	}

	public int getJ() {
		return j;
	}
	
	public int getKey() {
		return Tile.computeKey(i,j,level);
	}
    
    /**
     * Reclaims the bitmap.
     * Returns the bitmap if one was allocated.
     */
    public Bitmap reclaimBitmap() {
        Bitmap b = bitmap;
        bitmap = null;
        loaded = false;
        return b;
    }
    
    
    /** Used by TileThread to know if this tile is already in the memory list */
    public void setInMemory(boolean inM) {
        inMemory = inM;
    }

    /** Used by TileThread to know if this tile is already in the memory list */
    public boolean isInMemory() {
        return inMemory;
    }

	//called by tileThread
    public int load() {
    	if (loaded) return 0; //image already loaded
    	Bitmap bmp;
        String level_s=Integer.toHexString(level).toUpperCase();
        String i_s=Integer.toString(i);
        if (i<10) i_s="0"+i_s;
        if (i<100) i_s="0"+i_s;
        String j_s=Integer.toString(j);
        if (j<10) j_s="0"+j_s;
        if (j<100) j_s="0"+j_s;
        String name="S"+level_s+"/"+j_s+i_s+".jpg";
    	try {
            //name=String.format("S%X/%03d%03d.jpg",l,j,i); //upper code is 7x faster    		
    		bmp=BitmapFactory.decodeFile(Tile.dir+name);
    	} catch (OutOfMemoryError e) {
    		//leave loaded as false - thread can free memory and try again
    		return 2; //out of memory
    	}
    	if (bmp==null) {
    		if (Util.DEBUG) Util.logd("Loading image %s failed",name);
    		loaded=true; //we cannot change this image (this prevents from loading again)
    	}
        bitmap=bmp;
		loaded=true;
		return 0; //everything OK
    }
    
    public void setBitmap(Bitmap bmp) {
    	if (bmp==null && bitmap!=null) {
    		Util.logd("MEMOMRY LEAK");
    	}
        bitmap=bmp;
		loaded=true;
    }

}

