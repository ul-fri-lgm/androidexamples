package lgm.geo;

public class GpsUtil {

		//wgs84 specific
		private static double wgs84_a=6378137.0; //m
		private static double wgs84_a2=40680631590769.0;
		private static double wgs84_b=6356752.314245;	//m
		private static double wgs84_b2=40408299984659.2;
		private static double wgs84_e2=0.00669437999019758;	//e^2
		private static double wgs84_e2_=0.00673949674233346;	//e'^2;
		
		//bessel 1841 specific
		private static double bessel_a=6377397.155000;	//m
		private static double bessel_a2=40671194472602.1;	//a^2
		private static double bessel_b=6356078.96325;	//m
		private static double bessel_b2=40399739787069.2;
		private static double bessel_e2=0.00667437209683532;	//e^2
		private static double bessel_e2_=0.0067192186623881;	//e'^2
		
		//conversion between bessel 1841 and wgs84
		private static double dX=-409.545; //m //-409.520465;
		private static double dY=-72.164; //m //-72.191827;
		private static double dZ=-486.872; //m //-486.872387;
		private static double dm=-17.919665e-6; //scale //-17.919456e-6;
		private static double Alfa=3.085957; //seconds [''] //1.49625622332431e-05; //radians
		private static double Beta=5.469110; //seconds [''] 2.65141935723559e-05;
		private static double Gama=-11.020289; //seconds [''] -5.34282614688910e-05;
		
		//factor for series to determine fi0
		private static double A=1.0050373059464; //1.00503730599692;
		private static double B=0.00252392456913201;//0.00252392459157570; B/2
		private static double C=2.64094660090312e-6;//2.64094456224583e-6; C/4
		private static double D=3.43888673750786e-9;//3.43836164444015e-9; D/6
		private static double E=4.85670007015088e-12;//4.76916455578838e-12; E/8
		private static double F=7.00285901606003e-15;//F/10
		
		//gauss krugger specific
		private static double gk_false_east=500000;
		private static double gk_false_north=-5000000;
		private static double gk_scale=0.9999;
		private static double gk_lamda0=0.261799387799149; //15� in radians
		
		//converts angle given in degrees 
		//from dd.dddddd� to dd�mm'ss.sssss''
		public static void toMinSec(double alfa,double[] out) {
			if (out==null || out.length<3) return;
			out[0]=Math.floor(alfa); //st
			out[1]=Math.floor((alfa-out[0])*60); //min
			out[2]=((alfa-out[0])*60-out[1])*60; //ss
		}
		
		//converts angle given in degrees 
		//from dd.dddddd� to dd�mm.mmmmm'
		public static void toMin(double alfa,double[] out) {
			if (out==null || out.length<2) return;
			out[0]=Math.floor(alfa); //st
			out[1]=(alfa-out[0])*60; //min
		}
		
		
		//transform from GK x,y coordinates in Fi,Lam on Bessel 1841 elipsoid
		public static void toFiLam_B(double[] in,double[] out) {
			if (in==null || in.length<2) return;
			if (out==null || out.length<2) return;
			//convert from relative to real coordinates 
			double x=(in[0]-gk_false_north)/gk_scale;
			double y=(in[1]-gk_false_east)/gk_scale;
		
			//calculate Fi1 - footpoint latitude
			double ab=bessel_a + bessel_b;
			double fi1 = (2.0*x)/ab; //first estimate

			double dif=1.0;
			double p1=bessel_a * (1.0 - bessel_e2);
			int n=25;
			while(Math.abs(dif)>0 && n>0) {
				double L=p1*(A*fi1-B*Math.sin(2*fi1)+C*Math.sin(4*fi1)-D*Math.sin(6*fi1)+E*Math.sin(8*fi1)-F*Math.sin(10*fi1));
				dif=(2.0*(x-L)/ab);
				fi1+=dif;
				n--;
			}
			
			double N=bessel_a/(Math.sqrt(1-bessel_e2*Math.pow(Math.sin(fi1),2)));
			double TanFI1=Math.tan(fi1);
			double Tan2FI1=Math.pow(TanFI1,2);
			double Tan4FI1=Math.pow(TanFI1,4);
			double CosFI1=Math.cos(fi1);
			double ni2=bessel_e2_*Math.pow(CosFI1,2);
			
			out[0]=fi1- //FI
				Math.pow(y,2)*TanFI1*(1+ni2)/(2*Math.pow(N,2))+
				Math.pow(y,4)*TanFI1*(5+3*Tan2FI1+6*ni2-6*TanFI1*ni2-3*Math.pow(ni2,2)-9*Tan2FI1*Math.pow(ni2,2))/(24*Math.pow(N,4))-
				Math.pow(y,6)*TanFI1*(61+90*Tan2FI1+45*Tan4FI1+107*ni2-162*Tan2FI1*ni2-45*Tan4FI1*ni2)/(720*Math.pow(N,6))+
				Math.pow(y,8)*TanFI1*(1385+3633*Tan2FI1+4095*Tan4FI1+157*Math.pow(TanFI1,6))/(40320*Math.pow(N,8));			

			out[1]=gk_lamda0+ //LAMBDA
				(y/(N*CosFI1))-
				((Math.pow(y,3)*(1+2*Tan2FI1+ni2))/(6*Math.pow(N,3)*CosFI1))+
				((Math.pow(y,5)*(5+28*Tan2FI1+24*Tan4FI1+6*ni2+8*Tan2FI1*ni2))/(120*Math.pow(N,5)*CosFI1))+
				((Math.pow(y,7)*(61+662*Tan2FI1+1320*Tan4FI1+720*Math.pow(TanFI1,6)))/(5040*Math.pow(N,7)*CosFI1));

			
			//convert from radians to degrees
			out[0]=out[0]*180/Math.PI;
			out[1]=out[1]*180/Math.PI;
		}
		
		
		//transform from Fi,Lam on Bessel 1841 elipsoid to x,y coordinates in GK 
		public static void toXY_B(double[] in, double[] out) {
			if (in==null || in.length<2) return;
			if (out==null || out.length<2) return;
			//convert ot radians
			double fi=in[0]*Math.PI/180;
			double lambda=in[1]*Math.PI/180;
			
			double dl=lambda-gk_lamda0;
			double dl2=Math.pow(dl,2);
			double dl3=Math.pow(dl,3);
			double dl4=Math.pow(dl,4);
			double dl5=Math.pow(dl,5);
			double dl6=Math.pow(dl,6);
			double dl8=Math.pow(dl,8);
			
			
			double N=bessel_a/Math.sqrt(1-bessel_e2*Math.pow(Math.sin(fi),2));
			//double SinFI=Math.sin(fi);
			double CosFI=Math.cos(fi);
			double TanFI=Math.tan(fi);
			//double Sin2FI=Math.pow(SinFI,2);
			double Cos2FI=Math.pow(CosFI,2);
			double Cos3FI=Math.pow(CosFI,3);
			double Cos4FI=Math.pow(CosFI,4);
			double Cos5FI=Math.pow(CosFI,5);
			double Cos6FI=Math.pow(CosFI,6);
			double Cos8FI=Math.pow(CosFI,8);
			double Tan2FI=Math.pow(TanFI,2);
			double Tan4FI=Math.pow(TanFI,4);
			double Tan6FI=Math.pow(TanFI,6);
			double nfi2=bessel_e2_*Cos2FI;
			double nfi4=Math.pow(nfi2,2);

			//L ... dolzina loka meridiana od ekvatorja do dane geografske �irine to�ke
			double L=bessel_a*(1-bessel_e2)*(A*fi-(B*Math.sin(2*fi))+(C*Math.sin(4*fi))-(D*Math.sin(6*fi))+(E*Math.sin(8*fi))-(F*Math.sin(10*fi)));


			out[0]= L+ //x
				(N*TanFI*Cos2FI*dl2/2)+
				(N*TanFI*Cos4FI*(5-Tan2FI+9*nfi2+4*nfi4)*dl4/24)+
				(N*TanFI*Cos6FI*(61-58*Tan2FI+Tan4FI+270*nfi2-330*Tan2FI*nfi2)*dl6/720)+
				(N*TanFI*Cos8FI*(1385-3111*Tan2FI+543*Tan4FI-Tan6FI)*dl8/40320);
			
			out[0]*=gk_scale;
			out[0]+=gk_false_north;

			out[1]= (N*CosFI*dl)+ //y
				(N*Cos3FI*(1-Tan2FI+nfi2)*dl3/6)+
				(N*Cos5FI*(5-18*Tan2FI+Tan4FI+14*nfi2-58*Tan2FI*nfi2)*dl5/120)+
				(N*Math.pow(CosFI,7)*(61-479*Tan2FI+179*Tan4FI-Math.pow(TanFI,6))*Math.pow(dl,7)/5040);
			
			out[1]*=gk_scale;
			out[1]+=gk_false_east;
		}
		
		//Converts from Fi,Lambda to X,Y,Z on Bessel 1841 ellipsoid
		//if is_wgs=true, conversion takes place on WGS84 ellipsoid
		//does not include height!
		public static void toKart(double[] in, double[] out, boolean is_wgs) {
			if (in==null || in.length<2) return;
			if (out==null || out.length<3) return;
			//convert to radians
			double fi=in[0]*Math.PI/180;
			double lambda=in[1]*Math.PI/180;
			
			double el_a=bessel_a;
			double el_a2=bessel_a2;
			double el_b2=bessel_b2;
			double el_e2=bessel_e2;
			if (is_wgs) {
				el_a=wgs84_a;
				el_a2=wgs84_a2;
				el_b2=wgs84_b2;
				el_e2=wgs84_e2;
			}
			double N=el_a/(Math.sqrt(1-el_e2*Math.pow(Math.sin(fi),2)));
			out[0]=N*Math.cos(fi)*Math.cos(lambda);
			out[1]=N*Math.cos(fi)*Math.sin(lambda);
			out[2]=((el_b2/el_a2)*N)*Math.sin(fi);
		}
		
		//convert from X,Y,Z to Fi,Lam on Bessel 1841 ellipsoid
		//is is_wgs is true then conversion takes place on WGS84 ellipsoid
		public static void toFiLam(double[] in,double[] out,boolean is_wgs) {//$X,$Y,$Z,$is_wgs=false) {
			if (in==null || in.length<3) return;
			if (out==null || out.length<2) return;
			double el_a=bessel_a;
			double el_b=bessel_b;
			double el_e2_=bessel_e2_;
			double el_e2=bessel_e2;
			if (is_wgs) {
				el_a=wgs84_a;
				el_b=wgs84_b;
				el_e2_=wgs84_e2_;
				el_e2=wgs84_e2;
			}
			
			double p=Math.sqrt(Math.pow(in[0],2)+Math.pow(in[1],2));
			double O=Math.atan2(in[2]*el_a,p*el_b);
			double SinO=Math.sin(O);
			double Sin3O=Math.pow(SinO,3);
			double CosO=Math.cos(O);
			double Cos3O=Math.pow(CosO,3);

			out[0]=Math.atan2(in[2]+el_e2_*el_b*Sin3O,p-el_e2*el_a*Cos3O);
			out[1]=Math.atan2(in[1],in[0]);
			
			//convert to degrees
			out[0]=(out[0]*180)/Math.PI;
			out[1]=(out[1]*180)/Math.PI;
		}
		
		//converts from x,y in gauss krugger to fi,lambda in wgs84
		public static void gk2gps(double[] in, double[] out) { //$x_gk,$y_gk) {
			if (in==null || in.length<2) return;
			if (out==null || out.length<2) return;
			//allocate memory
			double[] fi_lam_b=new double[2];
			double[] xyz_b=new double[3];
			double[] xyz_w=new double[3];
			//convert to Fi,Lambda on Bessel 1841
			GpsUtil.toFiLam_B(in,fi_lam_b);
			//convert to X,Y,Z on Bessel 1841
			GpsUtil.toKart(fi_lam_b,xyz_b,false);
			
			//convert to X,Y,Z on wgs84
			//use similarity transform [X1;Y1;Z1]=s*R*[X;Y;Z]+[Xt;Yt;Zt]
			//R rotation matrix
			double alfa=(Alfa*Math.PI)/(180*3600);
			double beta=(Beta*Math.PI)/(180*3600);
			double gama=(Gama*Math.PI)/(180*3600);
			double[][] R={
				{1,Math.sin(gama),-1*Math.sin(beta)},
				{-1*Math.sin(gama),1,Math.sin(alfa)},
				{Math.sin(beta),-1*Math.sin(alfa),1}
			};
			
			xyz_b[0]-=dX;
			xyz_b[1]-=dY;
			xyz_b[2]-=dZ;
			xyz_b[0]/=(1+dm);
			xyz_b[1]/=(1+dm);
			xyz_b[2]/=(1+dm);

			xyz_w[0]=xyz_b[0]-R[0][1]*xyz_b[1]-R[0][2]*xyz_b[2];
			xyz_w[1]=-1*R[1][0]*xyz_b[0]+xyz_b[1]-R[1][2]*xyz_b[2];
			xyz_w[2]=-1*R[2][0]*xyz_b[0]-R[2][1]*xyz_b[1]+xyz_b[2];

			//convert to Fi,Lamda on wgs84
			GpsUtil.toFiLam(xyz_w,out,true);
		}


		//converts from fi,lambda in wgs84 to x,y in gauss krugger
		public static void gps2gk(double[] in, double[] out) { //$fi_gps,$lambda_gps
			if (in==null || in.length<2) return;
			if (out==null || out.length<2) return;
			//allocate memory
			double[] xyz_w=new double[3];
			double[] xyz_b=new double[3];
			double[] fi_lam_b=new double[2];
			
			//convert to X,Y,Z on wgs84
			GpsUtil.toKart(in,xyz_w,true);

			//convert to X,Y,Z on bessel 1841
			//use similarity transform [Xw;Yw;Zw]=s*R*[Xb;Yb;Zb]+[Xt;Yt;Zt]
			//R rotation matrix
			double alfa=(Alfa*Math.PI)/(180*3600);
			double beta=(Beta*Math.PI)/(180*3600);
			double gama=(Gama*Math.PI)/(180*3600);
			double[][] R={
				{1,Math.sin(gama),-1*Math.sin(beta)},
				{-1*Math.sin(gama),1,Math.sin(alfa)},
				{Math.sin(beta),-1*Math.sin(alfa),1}
			};

			xyz_b[0]=xyz_w[0]+R[0][1]*xyz_w[1]+R[0][2]*xyz_w[2];
			xyz_b[1]=R[1][0]*xyz_w[0]+xyz_w[1]+R[1][2]*xyz_w[2];
			xyz_b[2]=R[2][0]*xyz_w[0]+R[2][1]*xyz_w[1]+xyz_w[2];

			xyz_b[0]=(xyz_b[0]*(1+dm))+dX;
			xyz_b[1]=(xyz_b[1]*(1+dm))+dY;
			xyz_b[2]=(xyz_b[2]*(1+dm))+dZ;
		
			//convert to Fi,Lambda on bessel 1841
			GpsUtil.toFiLam(xyz_b,fi_lam_b,false);

			//convert to X,Y in Gauss Krugger
			GpsUtil.toXY_B(fi_lam_b,out);
		}
}
