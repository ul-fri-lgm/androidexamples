package lgm.geo;

import android.util.Log;

public class Util {
	
	public static final boolean DEBUG=false;
	private static final String TAG = "Geopedia";
	
    public static void logd(String format, Object...args) {
        Log.d(TAG, String.format(format, args));
    }
    
}
