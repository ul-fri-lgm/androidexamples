package fri.lgm.and_compass;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends Activity {

	SensorManager manager;
	SensorEventListener listener;
	TextView t1,t2,t3;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        t1=(TextView) findViewById(R.id.textView1);
        t2=(TextView) findViewById(R.id.textView2);
        t3=(TextView) findViewById(R.id.textView3);
        
        manager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        
        List<Sensor> list = manager.getSensorList(Sensor.TYPE_ALL);
        for (Sensor s : list) {
        		Log.d("SENZOR",s.getName());
        }
        
        Sensor compass = manager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        listener = new SensorEventListener() {
			
			@Override
			public void onSensorChanged(SensorEvent event) {
				// TODO Auto-generated method stub
				float[] val = event.values;
				t1.setText(val[0]+"�");
				t2.setText(val[1]+"�");
				t3.setText(val[2]+"�");
			}
			
			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy) {}
		};
		manager.registerListener(listener,compass,SensorManager.SENSOR_DELAY_UI);
    }

    public void onPause() {
    		manager.unregisterListener(listener);
    }

    
}
