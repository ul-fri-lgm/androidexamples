package fri.lgm.profile;

import java.util.Arrays;
import java.util.Comparator;

public class CustomMap {

	private int[][] map;
	private int capacity;
	private int numElements;
	private boolean sorted;
	
	public CustomMap(int size) {
		map=new int[size][2];
		capacity=size;
		sorted=false;
		numElements=0;
	}


	public void sort() {
		Arrays.sort(map, new Comparator() {
		    public int compare(Object o1, Object o2) {
		        int[] el1 = (int[])o1;
		        int[] el2 = (int[])o2;
		        if (el1[0] < el2[0]) return -1;
		        if (el1[0] > el2[0]) return 1;
		        return 0;
		    }
		});
		sorted=true;
		
	}
	
	public boolean put(int key, int value) {
		if (numElements>=capacity) return false; //do not add - no more space
		map[numElements][0]=key;
		map[numElements][1]=value;
		sorted=false;
		numElements++;
		return true;
	}
	
	public int get(int key) {
		if (!sorted) return -1; //must be sorted
		//binary searc alg
		return key;
	}
	
}
