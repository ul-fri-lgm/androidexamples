package fri.lgm.profile;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.os.Debug;
import android.widget.TextView;

public class Activity6 extends Activity {

	//custom map sorted + fast read
	private TextView status;
	private int layoutID = R.layout.activity_activity6;
	private int textViewID = R.id.status6;
	private final static String PROFILE_NAME = "profile6";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutID);
        
        status = (TextView)  findViewById(textViewID);
        
        Thread t = new Thread(new Runnable() {
        	public void run() {
        		doWork();
        	};
        });
        status.setText("Working...");
        t.start();
    }

    private void doWork() {
    	if (MainActivity.doProfile) Debug.startMethodTracing(PROFILE_NAME);
    	long startTime = System.nanoTime();
    	CustomMapSorted map;
    	
    	//custom code
    	try {
			BufferedInputStream bin=new BufferedInputStream(new FileInputStream(MainActivity.PATH));
			DataInputStream din=new DataInputStream(bin);
			int num=din.readInt();
			map = new CustomMapSorted(num);
			byte[] buf=new byte[4096];
			int numProcessed=0;
			while (numProcessed<num) {
				int numBytes=bin.read(buf);
				if (numBytes<8) break;
				int pointer=0;
				while (pointer<numBytes) {
					final int key=((buf[pointer]& 0xFF)<<24) + ((buf[pointer+1]& 0xFF)<<16) + ((buf[pointer+2]& 0xFF)<<8)+ (buf[pointer+3]& 0xFF);
					pointer+=4;
					final int value=((buf[pointer]& 0xFF)<<24) + ((buf[pointer+1]& 0xFF)<<16) + ((buf[pointer+2]& 0xFF)<<8)+ (buf[pointer+3]& 0xFF);
					pointer+=4;	
					map.put(key, value);
					numProcessed++;
				}
			}
			bin.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
    	

    	//end custom code
    	
    	long endTime = System.nanoTime();
    	final long time = (endTime - startTime)/(1000*1000);
    	Debug.stopMethodTracing();
    	runOnUiThread(new Runnable() {
			public void run() {
				status.setText("Done - "+time+" ms");
			}
		});
    }
    

}
