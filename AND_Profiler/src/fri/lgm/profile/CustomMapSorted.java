package fri.lgm.profile;


public class CustomMapSorted {

	private int[][] map;
	private int capacity;
	private int numElements;
	
	public CustomMapSorted(int size) {
		map=new int[size][2];
		capacity=size;
		numElements=0;
	}
	
	public boolean put(int key, int value) {
		if (numElements>=capacity) return false; //do not add - no more space
		map[numElements][0]=key;
		map[numElements][1]=value;
		numElements++;
		return true;
	}
	
	public int get(int key) {
		//binary searc alg
		return key;
	}
}
