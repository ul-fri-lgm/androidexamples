package fri.lgm.profile;


public class CustomMapSortedSingle {

	private int[] map;
	private int capacity;
	private int numElements;
	
	public CustomMapSortedSingle(int size) {
		map=new int[2*size];
		capacity=size;
		numElements=0;
	}
	
	public boolean put(int key, int value) {
		if (numElements>=capacity) return false; //do not add - no more space
		int n=2*numElements;
		map[n]=key;
		map[n+1]=value;
		numElements++;
		return true;
	}
	
	public int get(int key) {
		//binary searc alg
		return key;
	}
}
