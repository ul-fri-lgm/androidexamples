package fri.lgm.profile;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.os.Bundle;
import android.os.Debug;
import android.widget.TextView;

public class Activity2 extends Activity {

	//HashMap
	private TextView status;
	private int layoutID = R.layout.activity_activity2;
	private int textViewID = R.id.status2;
	private final static String PROFILE_NAME = "profile2";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutID);
        
        status = (TextView)  findViewById(textViewID);
        
        Thread t = new Thread(new Runnable() {
        	public void run() {
        		doWork();
        	};
        });
        status.setText("Working...");
        t.start();
    }

    private void doWork() {
    	if (MainActivity.doProfile) Debug.startMethodTracing(PROFILE_NAME,30*1024*1024);
    	long startTime = System.nanoTime();
    	Map<Integer,Integer> map = new HashMap<Integer,Integer>();
    	
    	//custom code
    	try {
			DataInputStream fin = new DataInputStream(new FileInputStream(MainActivity.PATH));
			int num=fin.readInt();
			for (int i=0;i<num;i++) {
				int key=fin.readInt();
				int value=fin.readInt();
				map.put(key, value);
			}
			fin.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	

    	//end custom code
    	
    	long endTime = System.nanoTime();
    	final long time = (endTime - startTime)/(1000*1000);
    	Debug.stopMethodTracing();
    	runOnUiThread(new Runnable() {
			public void run() {
				status.setText("Done - "+time+" ms");
			}
		});
    }
    
}
