package fri.lgm.profile;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	private final static int PROBLEM_SIZE = 10000;
	public final static String PATH = Environment.getExternalStorageDirectory()+"/profilerTest.dat";
	private Button[] buttons;
	private Button genUnsorted;
	private Button genSorted;
	
	public static boolean doProfile=true;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttons = new Button[7];
        
        buttons[0] = (Button) findViewById(R.id.button1);
        buttons[1] = (Button) findViewById(R.id.button2);
        buttons[2] = (Button) findViewById(R.id.button3);
        buttons[3] = (Button) findViewById(R.id.button4);
        buttons[4] = (Button) findViewById(R.id.button5);
        buttons[5] = (Button) findViewById(R.id.button6);
        buttons[6] = (Button) findViewById(R.id.button7);
        
        genUnsorted = (Button) findViewById(R.id.buttonGU);
        genSorted = (Button) findViewById(R.id.buttonGS);
        
        File f = new File(PATH);
        if (!f.exists()) {
        	for (Button b : buttons) {
        		b.setEnabled(false);
        	}
        }
    }

    public void generateFile(View w) {
    	String state = Environment.getExternalStorageState();
    	if (!Environment.MEDIA_MOUNTED.equals(state)) {
    		Toast.makeText(this,"Cannot create file on SD",Toast.LENGTH_LONG).show();
    		return;
    	}
		Toast.makeText(this,"Generating...",Toast.LENGTH_SHORT).show();
    	Log.d("PROFILE","generating..");
    	genUnsorted.setEnabled(false);
    	genSorted.setEnabled(false);
		for (Button b : buttons) {
			b.setEnabled(false);
		}
		final boolean sorted = (w == genUnsorted)?false:true; //sort or random
		Thread t = new Thread(new Runnable() {
			public void run() {
		    	try {
		    		Random r = new Random();
					DataOutputStream fout = new DataOutputStream(new FileOutputStream(PATH));
					Map<Integer,Integer> map = new TreeMap<Integer,Integer>();
					for (int i=0;i<PROBLEM_SIZE;i++) {
						int key=i;
						if (!sorted) {
							key=r.nextInt();
						}
						int value=r.nextInt();
						map.put(key, value);
					}

					fout.writeInt(map.keySet().size());
					if (sorted) {

						for (int i=0;i<PROBLEM_SIZE;i++) {
							fout.writeInt(i);
							fout.writeInt(map.get(i));
						}
					} else {
						for (Integer key : map.keySet()) {
							fout.writeInt(key);
							fout.writeInt(map.get(key));
						}
					}
					fout.close();
					runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(MainActivity.this,"Generated",Toast.LENGTH_SHORT).show();
					    	genUnsorted.setEnabled(true);
					    	genSorted.setEnabled(true);
							for (Button b : buttons) {
								b.setEnabled(true);
							}
						}
					});
				} catch (IOException e) {
					runOnUiThread(new Runnable() {
						public void run() {
							Toast.makeText(MainActivity.this,"ERROR",Toast.LENGTH_SHORT).show();
						}
					});
					e.printStackTrace();
				}
				
			}
		});
		t.start();

    }

    public void zazeniAktivnost(View w) {
    	Intent intent = null;
    	if (w == buttons[0]) {
    		intent = new Intent(this,Activity1.class);
    	} else if (w == buttons[1]) {
    		intent = new Intent(this,Activity2.class);
    	} else if (w == buttons[2]) {
    		intent = new Intent(this,Activity3.class);
    	} else if (w == buttons[3]) {
    		intent = new Intent(this,Activity4.class);
    	} else if (w == buttons[4]) {
    		intent = new Intent(this,Activity5.class);
    	} else if (w == buttons[5]) {
    		intent = new Intent(this,Activity6.class);
    	} else if (w == buttons[6]) {
    		intent = new Intent(this,Activity7.class);
    	}
		if (intent!=null) {
			startActivity(intent);
		} else {
	    	Log.d("PROFILE","activity ne dela");
		}
    }
    
    public void changeDebug(View w) {
    	CheckBox cb = (CheckBox) w;
    	doProfile=!cb.isChecked();
    }

    
}
