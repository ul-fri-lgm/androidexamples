package fri.lgm.profile;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.os.Debug;
import android.util.SparseIntArray;
import android.widget.TextView;

public class Activity3 extends Activity {

	//SparseIntArray
	private TextView status;
	private int layoutID = R.layout.activity_activity3;
	private int textViewID = R.id.status3;
	private final static String PROFILE_NAME = "profile3";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutID);
        
        status = (TextView)  findViewById(textViewID);
        
        Thread t = new Thread(new Runnable() {
        	public void run() {
        		doWork();
        	};
        });
        status.setText("Working...");
        t.start();
    }

    private void doWork() {
    	if (MainActivity.doProfile) Debug.startMethodTracing(PROFILE_NAME);
    	long startTime = System.nanoTime();
    	SparseIntArray map = new SparseIntArray();
    	
    	//custom code
    	try {
			DataInputStream fin = new DataInputStream(new FileInputStream(MainActivity.PATH));
			int num=fin.readInt();
			for (int i=0;i<num;i++) {
				int key=fin.readInt();
				int value=fin.readInt();
				map.put(key, value);
			}
			fin.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    	

    	//end custom code
    	
    	long endTime = System.nanoTime();
    	final long time = (endTime - startTime)/(1000*1000);
    	Debug.stopMethodTracing();
    	runOnUiThread(new Runnable() {
			public void run() {
				status.setText("Done - "+time+" ms");
			}
		});
    }
    

    
}
