package lgm.fri;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;


public class ImageView extends View {

	//bitmap values
	private Bitmap bitmap=null;
	private int bitmapSizeX=0;
	private int bitmapSizeY=0;
		
	private float Ax=0;
	private float Ay=0;
	private float Bx=0;
	private float By=0;
	private double distAB=0.0;
	private double anglAB=0.0;
	
	//image translation parameters
	private Matrix imageMatrix=new Matrix();
	private boolean imageScaleSet=false;
	private int viewSizeX=0;
	private int viewSizeY=0;
	
	public ImageView(Context context, AttributeSet attrs) {
		super(context,attrs);
		try {
			
			bitmap=BitmapFactory.decodeResource(context.getResources(), R.drawable.slika);
    	} catch (OutOfMemoryError e) {
    		Log.d("IMAGETEST","Out of memory!");
    	}
    	if (bitmap!=null) {
    		Log.d("IMAGETEST","Image loaded!");
    		bitmapSizeX=bitmap.getWidth();
    		bitmapSizeY=bitmap.getHeight();
    	} else {
    		Log.d("IMAGETEST","Ni slike?");
    	}
		
	}

	protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (bitmap!=null) {
        	canvas.drawBitmap(bitmap,imageMatrix,null /*paint*/);
        }
	}
	
	void offsetView(float dx,float dy) {
		imageMatrix.postTranslate(dx,dy);
	}
	
	void scaleRotateView(float scale, float rotate,float centerX, float centerY) {
		imageScaleSet=true;
		imageMatrix.postScale(scale,scale,centerX,centerY);
		imageMatrix.postRotate(rotate,centerX,centerY);
	}
	
	
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {    	
        super.onSizeChanged(w, h, oldw, oldh);
        viewSizeX=w;
        viewSizeY=h;
        if (!imageScaleSet) {
        	//change image scale if not already set!
        	float scale=1.0f*viewSizeX/bitmapSizeX;
        	float scaleY=1.0f*viewSizeY/bitmapSizeY;
        	if (scaleY<scale) scale=scaleY; //set scale to smaller of both!
        	imageMatrix.postScale(scale,scale);
        }
    }    

    public boolean onTouchEvent(MotionEvent event) {
    	float dx,dy;
    	int actionCode = event.getAction() & MotionEvent.ACTION_MASK;
        switch (actionCode) {
        	case MotionEvent.ACTION_DOWN:
        		Ax=event.getX();
        		Ay=event.getY();
        		return true;
        	case MotionEvent.ACTION_POINTER_DOWN:
        		Ax=event.getX(0);
        		Ay=event.getY(0);
    	    	Bx=event.getX(1);
    	    	By=event.getY(1);
    	    	dx=Ax-Bx;
    	    	dy=Ay-By;
    	    	distAB=Math.hypot(dx,dy);
    	    	anglAB=Math.atan2(dy,dx);
        		return true;
        	case MotionEvent.ACTION_POINTER_UP:
        		
        		if (event.getPointerCount()==2) { //count==2 if two fingers were down, but now one has gone up
        			//in case of a lift of "wrong finger" we need to switch previous position of first finger so drag is working as expected
        			int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_ID_MASK) >> MotionEvent.ACTION_POINTER_ID_SHIFT;
					int remId = 1-event.getPointerId(pointerIndex); //since we only have two fingers 
					//try catch because there can still be problems - with two fingers bug?, in case of three or more more common
					try {
						Ax = event.getX(remId);
						Ay = event.getY(remId);
					} catch (Exception e) {
						Log.d("IMAGETEST","Finger kung-fu failed: "+e.getMessage());
						Ax = event.getX(0); //we take finger with id=0 (always exists)
						Ay = event.getY(0);
					}
        		}
        		
				return true;
        	case MotionEvent.ACTION_UP:
            	return true;
        	case MotionEvent.ACTION_MOVE:
    	    	float x1=event.getX(0);
    	    	float y1=event.getY(0);
    	    	dx = x1 - Ax;
    	    	dy = y1 - Ay;
    	    	offsetView(dx,dy); //move pixel under first finger to new location
    	    	Ax=x1;
    	    	Ay=y1;
    	    	if (event.getPointerCount()>1) {
    	    		//in case of second finger determine scale and rotation that pixel under second finger matches new location
        	    	float x2=event.getX(1);
        	    	float y2=event.getY(1);
        	    	double dist=Math.hypot(x1-x2,y1-y2);
        	    	double angle=Math.atan2(y1-y2, x1-x2);
        	    	float scale=(float) (dist/distAB);
        	    	float rotate=(float) (180*(angle-anglAB)/Math.PI);
        	    	scaleRotateView(scale,rotate,x1,y1);
            	    Bx=x2;
            	    By=y2;
            	    distAB=dist;
            	    anglAB=angle;
        		}
    	    	invalidate(); //ask for draw event on view
        		return true;
        }
        return super.onTouchEvent(event);
    }	    
    
}
