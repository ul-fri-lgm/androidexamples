package fri.android.audio;

import android.app.Activity;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;

public class MyActivity extends Activity
{
    public static final String PATH = "/sdcard/media/recording.3gp";
    private MediaRecorder rec;
    private MediaPlayer mp;
    private State state = State.STOPPED;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mp = new MediaPlayer();
        rec = new MediaRecorder();
        try {
            rec.setAudioSource(MediaRecorder.AudioSource.MIC);
            rec.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
            rec.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            rec.setOutputFile(PATH);
        } catch (Exception e)  {}
    }

    public void record(View v) {
        if(state!=State.STOPPED) {
            msg("not ready");
            return;
        }

        try {
            rec.prepare();
            rec.start();
            state = State.RECORDING;
        } catch (Exception e)  {
            msg(e.toString());
        }

    }

    public void play(View v) {
        if(state!=State.STOPPED) {
            msg("not ready");
            return;
        }

        if(!new File(PATH).exists()) {
            msg("nothing to play");
            return;
        }

        try {
            mp.setDataSource(PATH);
            mp.prepare();
            mp.start();
            state=State.PLAYING;
        } catch (Exception e) {	}
    }

    public void stop(View v) {
        if(state==State.STOPPED) {
            msg("not running");
            return;
        }
        stopInternal();
    }

    private void stopInternal() {
        try {
            mp.pause();
            mp.stop();
        } catch(Exception e) {}

        try {
            rec.stop();
            rec.release();
        } catch(Exception e) {}
        state = State.STOPPED;
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopInternal();
    }

    private void msg(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    private enum State{
        PLAYING, RECORDING, STOPPED
    }

}
