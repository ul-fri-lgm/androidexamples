package fri.android;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.content.Context;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Toast;

class Preview extends SurfaceView implements SurfaceHolder.Callback, View.OnClickListener, Camera.ShutterCallback, Camera.PictureCallback {
    SurfaceHolder mHolder;
    Camera mCamera;
    private boolean running;

    Preview(Context context) {
        super(context);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        this.setOnClickListener(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // The Surface has been created, acquire the camera and tell it where
        // to draw.
        mCamera = Camera.open();
        try {
            mCamera.setPreviewDisplay(holder);
        } catch (IOException exception) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // Surface will be destroyed when we return, so stop the preview.
        // Because the CameraDevice object is not a shared resource, it's very
        // important to release it when the activity is paused.
        mCamera.stopPreview();
        mCamera = null;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // Now that the size is known, set up the camera parameters and begin
        // the preview.
        Camera.Parameters parameters = mCamera.getParameters();
        parameters.setPreviewSize(w, h);
        mCamera.setParameters(parameters);
        mCamera.startPreview();
        running=true;
    }


    @Override
    public void onClick(View view) {
        //take photo or resume preview
        if(running) {
            mCamera.takePicture(this, null, this);
            running=false;
        } else {
            mCamera.startPreview();
            running=true;
        }
    }

    @Override
    public void onShutter() {
        Toast.makeText(getContext(), "photo taken", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPictureTaken(byte[] bytes, Camera camera) {
        new SavePhotoTask().doInBackground(bytes);
    }

    class SavePhotoTask extends AsyncTask<byte[], Void, String> {
        @Override
        protected String doInBackground(byte[]... jpeg) {
            File photo=new File("/sdcard", "photo.jpg");

            if (photo.exists()) {
                photo.delete();
            }
            String path = photo.getPath();

            try {
                FileOutputStream fos=new FileOutputStream(path);

                fos.write(jpeg[0]);
                fos.close();
            }
            catch (java.io.IOException e) {
                Log.e("PictureDemo", "Exception in photoCallback", e);
            }

            return(null);
        }
    }
}