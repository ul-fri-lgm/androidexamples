package and.hello;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

public class MapView extends View {

	private static final String TAG = "Hello";
	private static final int DELTA = 30;

	Bitmap bmp=null;
	float down_x,down_y;
	int view_w,view_h;
	int window_offset_x=0,window_offset_y=0,image_w,image_h;
	double zoom_factor=0.1;
	
	public MapView(Context context, AttributeSet attrs) {
		super(context,attrs);
//		int w=512;
//		int h=512;
//		int[] buffer=new int[w*h];
//		for (int y=0;y<h;y++) {
//			for (int x=0;x<w;x++) {
//				buffer[y*w+x]=y+x;
//			}
//		}
//		bmp=Bitmap.createBitmap(buffer,w,h,Bitmap.Config.RGB_565);
		
	}
	
	public void init(Bundle savedInstanceState) {
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus(); //for keyevent to go to this view
		if (savedInstanceState==null) return;
		window_offset_x=savedInstanceState.getInt("window_offset_x",0);
		window_offset_y=savedInstanceState.getInt("window_offset_y",0);
		zoom_factor=savedInstanceState.getDouble("zoom_factor",0.1);
	}
	
	public void saveState(Bundle outState) {
		outState.putInt("window_offset_x",window_offset_x);
		outState.putInt("window_offset_y",window_offset_y);
		outState.putDouble("zoom_factor",zoom_factor);
	}
		
	public void loadImage(Bitmap image) {
		if (image!=null) {
			bmp=image;
			image_w=bmp.getWidth();
			image_h=bmp.getHeight();
		}
	}
	
	public Bitmap getImage() {
		return bmp;
	}
	
	public void unloadImage() {
		if (bmp!=null) {
			bmp.recycle();
		}
	}
	
	
	@Override
    protected void onDraw(Canvas canvas) {
		
        super.onDraw(canvas);

		long t1=System.nanoTime();
//    	Paint p=new Paint();			
//    	p.setStyle(Paint.Style.STROKE);
//		p.setStrokeWidth(1);
//		p.setColor(Color.WHITE);
//		p.setTextSize(30);
//		String size=Integer.toString(getWidth())+"x"+Integer.toString(getHeight());
//    	canvas.drawText(size,50,50,p);
        
        
        if (bmp!=null) {
        	//canvas.drawBitmap(bmp,0,0, null);

        	int display_w=view_w;
        	int display_h=view_h;
        	int window_w=(int) (view_w/zoom_factor); //default window size (w)
        	int window_h=(int) (view_h/zoom_factor); //default window size (h)
       		if (window_w>image_w) {
       			window_offset_x=0;
       			window_w=image_w;
       			display_w=(int) (zoom_factor*image_w);
       		}
       		if (window_h>image_h) {
       			window_offset_y=0;
       			window_h=image_h;
       			display_h=(int) (zoom_factor*image_h);
       		}
       		if (window_offset_x+window_w>image_w) {
       			window_offset_x=image_w-window_w;
       		}
       		if (window_offset_y+window_h>image_h) {
       			window_offset_y=image_h-window_h;
       		}

        	//Log.i(TAG,"Window: "+window_offset_x+","+window_offset_y+","+window_w+","+window_h);
        	//Log.i(TAG,"Display: "+display_w+","+display_h);
			Rect windowRect=new Rect(window_offset_x,window_offset_y,window_offset_x+window_w,window_offset_y+window_h);
			Rect displayRect=new Rect(0,0,display_w,display_h);
        	canvas.drawBitmap(bmp,windowRect,displayRect,null);
    		
        	long t2=System.nanoTime();
    		double time=(1.0*t2-t1)/(1000*1000);
    		Log.i(TAG,"drawing "+time);        	
        }
    }
    
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        view_w=w;
        view_h=h;
        //Log.i(TAG,"New view size "+w+"x"+h);
    }
    
    
    private void changeOffset(int dx, int dy) {
    	int off_x=window_offset_x+dx;
    	if (off_x<0) off_x=0;
    	if (off_x+view_w>image_w) off_x=image_w-view_w; 
    	int off_y=window_offset_y+dy;
    	if (off_y<0) off_y=0;
    	if (off_y+view_h>image_h) off_y=image_h-view_h;
    	window_offset_x=off_x;
    	window_offset_y=off_y;
    	postInvalidate();
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
        	case MotionEvent.ACTION_DOWN:
        		down_x = event.getX();
        		down_y = event.getY();
        		return true;
            case MotionEvent.ACTION_UP:
            	int off_x = (int)(down_x - event.getX());
            	int off_y = (int)(down_y - event.getY());
        		if (Math.abs(off_x)+Math.abs(off_y)>10) {
            		//touch as drag
            		//Log.i(TAG,"dragged");	
            		changeOffset(off_x,off_y);
        		} else {
        			//touch as click
        			//Log.i(TAG,"clicked");
        			int curr_center_x=view_w/2;
        			int curr_center_y=view_h/2;
        			int new_center_x=(int) event.getX();
        			int new_center_y=(int) event.getY();
        			//Log.i(TAG,"old center: "+curr_center_x+","+curr_center_y);
        			//Log.i(TAG,"new center: "+new_center_x+","+new_center_y);
        			changeOffset(new_center_x-curr_center_x,new_center_y-curr_center_y);
        		}
        		return true;
        }
        return super.onTouchEvent(event);
    }
    
    @Override
    public boolean  onKeyDown (int keyCode, KeyEvent event) {
    	switch (keyCode) {
    		case KeyEvent.KEYCODE_DPAD_DOWN:
    			changeOffset(0,DELTA);
    			return true;
    		case KeyEvent.KEYCODE_DPAD_UP:
    			changeOffset(0,-DELTA);
    			return true;
    		case KeyEvent.KEYCODE_DPAD_LEFT:
    			changeOffset(-DELTA,0);
    			return true;
    		case KeyEvent.KEYCODE_DPAD_RIGHT:
    			changeOffset(DELTA,0);
    			return true;
    		case KeyEvent.KEYCODE_BACK:
    			zoom_factor*=0.9;
    	    	postInvalidate();
    			return true;
    		case KeyEvent.KEYCODE_SEARCH:
    			zoom_factor*=(1.0/0.9);
    	    	postInvalidate();
    			return true;
    	}
    	return super.onKeyDown(keyCode,event);
    }

}
