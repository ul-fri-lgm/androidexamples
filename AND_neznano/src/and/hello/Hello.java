package and.hello;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

public class Hello extends Activity {
	
	private static final String TAG = "Hello";
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.w(TAG,"Created");
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.test);
        
        MapView mapView = (MapView) findViewById(R.id.map_view);
        mapView.init(savedInstanceState);
        setImage(mapView);
//        Thread t=new Thread(new Runnable() {
//			
//			public void run() {
//				try {
//					for (;;) {
//						mapView.loadImage();
//						Thread.sleep(300);
//					}
//				} catch (Exception e) {
//					// TODO: handle exception
//				}
//			}
//		});
//        t.start();
    }
    
    private void setImage(MapView view) {
    	Object data = getLastNonConfigurationInstance();
    	Bitmap image=null;
    	if (data!=null) {
    		image=(Bitmap) data;
    		if (image.isRecycled()) image=null; //test if image has been recycled
    	}
    	if (image==null) {
    		String img_name=Environment.getExternalStorageDirectory().toString()+"/images/drevo.jpg";
//    		BitmapFactory.Options opt=new BitmapFactory.Options();
//    		opt.inInputShareable=true;
//    		opt.inPurgeable =true;
//    		Bitmap image=BitmapFactory.decodeFile(img_name,opt);
    		image=BitmapFactory.decodeFile(img_name);
    		Log.w(TAG,"loading image");
    		
    	}
		view.loadImage(image);
    }
    
    public void onRestart() {
    	super.onRestart();
    	Log.i(TAG,"onRestart");
    }
    
    public void onStart() {
    	super.onStart();
    	Log.i(TAG,"onStart");
    	
    }
    
    public void onStop() {
    	super.onStop();
    	Log.i(TAG,"onStop");
    }
    
    public void onResume() {
    	super.onResume();
    	Log.i(TAG,"onResume");
    }
    
    public void onPause() {
    	super.onPause();
    	Log.i(TAG,"onPause");
    }
    
    public void onDestroy() {
    	super.onDestroy();
    	Log.i(TAG,"onDestroy");
    	//call mapview.unloadImage();
    }
    
    public void onSaveInstanceState(Bundle outState) {
    	super.onSaveInstanceState(outState);
    	MapView mapView = (MapView) findViewById(R.id.map_view);
    	mapView.saveState(outState);
    	Log.i(TAG,"SavedInstance");
    	
    }
    
    @Override
    public Object onRetainNonConfigurationInstance() {
    	MapView mapView = (MapView) findViewById(R.id.map_view);
    	Bitmap bmp=mapView.getImage();
    	Log.i(TAG,"onRetainNonConfigurationInstance");
        return bmp;
    }
    
    
}