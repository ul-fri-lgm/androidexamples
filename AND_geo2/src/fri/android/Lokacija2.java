package fri.android;

import java.util.List;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;

public class Lokacija2 extends Activity {
		
	private LocationManager locationManager;
	private LocationListener locationListener;
	private TextView tw;
	private boolean isListenerRegistered;
	private int counter=0;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        tw=(TextView) findViewById(R.id.locationLabel);
    	locationManager=(LocationManager) getSystemService(LOCATION_SERVICE);
    	locationListener=new LocationListener() {
			
			public void onStatusChanged(String provider, int status, Bundle extras) {}
			public void onProviderEnabled(String provider) {}
			public void onProviderDisabled(String provider) {}
			
			public void onLocationChanged(Location location) {
				updateLocation(location);
			}
		};
    	List<String> providers=locationManager.getProviders(true);
    	if (providers.size()>0) {
    		long minTime=0; //every s
    		float minDistance=0; //no need to change location
    		locationManager.requestLocationUpdates(providers.get(0),minTime, minDistance,locationListener);
    		isListenerRegistered=true;
    	} else {
    		isListenerRegistered=false;
    	}
    }
    
    public void updateLocation(Location location) {
		counter++;
        String s=getString(R.string.lokacija);
    	double lat=location.getLatitude();
		double lon=location.getLongitude();
		s+=lat+","+lon+" ["+counter+"]";
		tw.setText(s);
    }
    
    public void onPause() {
    	super.onPause();
    	if (isListenerRegistered) {
    		locationManager.removeUpdates(locationListener);
    		isListenerRegistered=false;
    	}
    }
}