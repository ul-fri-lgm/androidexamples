package fri.lgm.slikica;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.widget.ImageView;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        ImageView iv = (ImageView) findViewById(R.id.image_view);
        Bitmap bitmap = Bitmap.createBitmap(200,200,Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bitmap);
        Paint p = new Paint();
        p.setColor(0xffff0000);
        c.drawRect(new Rect(10,10,100,100), p);
        
		iv.setImageBitmap(bitmap);
        
    }

    
}
