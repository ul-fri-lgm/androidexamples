package fri.lgm.slikica;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class MojPogled extends View {

	private Paint p;
	
	public MojPogled(Context context, AttributeSet attrs) {
		super(context, attrs);
		p = new Paint();
		p.setColor(Color.BLUE);
	}
	
	protected void onDraw(Canvas c) {
		super.onDraw(c);
		int w = c.getWidth();
		int h = c.getHeight();
		int cx = w/2;
		int cy = h/2;
		int r = 50;
		c.drawCircle(cx, cy, r, p);
	}

}
