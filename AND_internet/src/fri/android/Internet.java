package fri.android;

import java.io.InputStream;
import java.net.URL;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class Internet extends Activity {
	
	TextView internetStatus;
	Button dlButton;
	BroadcastReceiver internetStatusReceiver;

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        internetStatus = (TextView) findViewById(R.id.textContainer);
        internetStatus.setTextSize(50);
        
        dlButton = (Button) findViewById(R.id.button1);
        
        displayInternetStatus();
        internetStatusReceiver = new BroadcastReceiver() {
			
    		@Override
    		public void onReceive(Context context, Intent intent) {
    			if (!intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) return;
                String reason = intent.getStringExtra(ConnectivityManager.EXTRA_REASON);
    			boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
                String extra = intent.getStringExtra(ConnectivityManager.EXTRA_EXTRA_INFO);
                boolean isFailover = intent.getBooleanExtra(ConnectivityManager.EXTRA_IS_FAILOVER, false);
                NetworkInfo otherNetworkInfo = (NetworkInfo) intent.getParcelableExtra(ConnectivityManager.EXTRA_OTHER_NETWORK_INFO);
                String oni = (otherNetworkInfo==null)?"Null":otherNetworkInfo.getTypeName();
                
    			Log.d("INTERNET","jeNet: "+(noConnectivity?"Ne":"Ja")+" failover: "+(isFailover?"Ne":"Ja") + " razlog: "+reason+ " drugaMreza: "+oni +" ostalo: " + extra);
    			displayInternetStatus();
    		}
    	};
        //register listener
        registerReceiver(internetStatusReceiver,new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
	}
	

	public void displayInternetStatus() {
        ConnectivityManager conMan= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo  ni =  conMan.getActiveNetworkInfo();
        if (ni == null || !ni.isConnected()) {
        	dlButton.setEnabled(false);
        	internetStatus.setText("NI INTERNETA");
        } else {
        	dlButton.setEnabled(true);
        	switch (ni.getType()) {
        		case ConnectivityManager.TYPE_WIFI:
        			//smo v poceni tarifi � prena�aj dolge datoteke
        			internetStatus.setText("POCENI");
        			break;
        		case ConnectivityManager.TYPE_MOBILE:
        			//smo v poceni tarifi � prena�aj dolge datoteke
        			internetStatus.setText("DRAGO"); 
        			break;
        	}   
        }
	}
	
	public void setImage(Bitmap b) {
    	ImageView imageView=(ImageView) findViewById(R.id.imageContainer);
		imageView.setImageBitmap(b);
		internetStatus.setText("DONE!");
    	dlButton.setEnabled(false);
		unregisterReceiver(internetStatusReceiver);
		
	}
	
	public void loadImage(View v) {
		internetStatus.setText("LOADING...");
		
		Thread t = new Thread(new Runnable() {
			
			public void run() {
		    	try {
		    		URL url = new URL("http://www.fri.uni-lj.si/file/31081/ocivghqtbx.jpg");
		    		InputStream in=url.openStream();
		    		final Bitmap bitmap = BitmapFactory.decodeStream(in);
		    		runOnUiThread(new Runnable() {
						public void run() {
							setImage(bitmap);
						}
					});
		    	} catch (Exception e) {
		    		runOnUiThread(new Runnable() {
						public void run() {
							internetStatus.setText("ERROR...");
						}
					});
		       		Log.e("internet","failed: "+e.getMessage()); 
		       	}
			}
		});
		t.start();
    	
	}
}