package fri.android;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MotionEvent;

public class SoundRecorder extends Activity {
	boolean recording = true;
	private MediaRecorder recorder;
    private MediaPlayer mp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        startRecording();
    }
    
    private void startRecording() {
        try {
	    	recorder = new MediaRecorder();	        
	        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
	        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
	        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
	        recorder.setOutputFile("/sdcard/media/test.3gp");
	        
	        recorder.prepare();
	        recorder.start();
        } catch (Exception e) {
		}
    }
    
    private void stopRecordingAndPlay() {
        try {
        	recorder.stop();
        	recorder.release();
        	
			mp = new MediaPlayer();
			mp.setDataSource("/sdcard/media/test.3gp");
			mp.prepare();
			mp.start();
        } catch (Exception e) {	}
    }

    @Override
    public void onPause() {
    	super.onPause();
    	mp.stop();
    	mp.release();
    }

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			if (recording) {
				stopRecordingAndPlay();
			} else {
				startRecording();
			}
		}
		return true;
	}
}