package fri.android;

import java.util.List;

import android.app.Activity;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;

public class Lokacija extends Activity {
		
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        TextView tw=(TextView) findViewById(R.id.locationLabel);
        String s=getString(R.string.lokacija);
        
    	LocationManager locationManager=(LocationManager) getSystemService(LOCATION_SERVICE);
    	List<String> providers=locationManager.getProviders(true);
    	if (providers.size()>0) {
    		Location location=locationManager.getLastKnownLocation(providers.get(0));
    		double lat=location.getLatitude();
    		double lon=location.getLongitude();
    		
    		s+=lat+","+lon;
    	} else {
    		s+="brez lokacije";
    	}
		tw.setText(s);
    }
    
}