package fri.lgm.and_location;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends Activity {

	TextView statusView;
	LocationManager manager;
	LocationListener listener;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        statusView = (TextView) findViewById(R.id.textView1);
       
        manager=(LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener = new LocationListener() {

			@Override
			public void onLocationChanged(Location location) {
				// TODO Auto-generated method stub
				statusView.setText("Nova lokacija "+location);
			}

			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub
				statusView.setText("Ni vec "+provider);
			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub
				statusView.setText("Je zraven "+provider);
				
			}

			@Override
			public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
				// TODO Auto-generated method stub
				
			}
        	
        };
        manager.requestLocationUpdates(
        		LocationManager.GPS_PROVIDER,0,0,listener
        	);
        manager.requestLocationUpdates(
        		LocationManager.NETWORK_PROVIDER,0,0,listener
        	);
    }


    public void onPause() {
    		manager.removeUpdates(listener);
    }

    
}
