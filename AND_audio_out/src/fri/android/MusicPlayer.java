package fri.android;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;

public class MusicPlayer extends Activity {
    private MediaPlayer mp;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
//        mp = MediaPlayer.create(this, R.raw.thesedays);
//        mp.start();
//        mp.pause();
//        mp.stop();
        
        try {
			mp = new MediaPlayer();
			mp.setDataSource("/sdcard/media/thesedays.mp3");
			mp.prepare();
			mp.start();
        } catch (Exception e) {	}
 
    }

    @Override
    public void onPause() {
    	super.onPause();
    	mp.pause();
    }

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			if (mp.isPlaying())
				mp.pause();
			else
				mp.start();
		}
		return true;
	}
}