package fri.lgm.and_slikar;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class DrawView extends View implements OnTouchListener {

	Paint paint = new Paint();
	ArrayList<Tocka> tocke = new ArrayList<Tocka>();
	
	public DrawView(Context context) {
		super(context);
		paint.setColor(Color.GREEN);
		this.setOnTouchListener(this);
	}

	@Override
	protected void onDraw(Canvas c) {
		for(Tocka t : tocke) {
			c.drawCircle(t.x, t.y, t.pressure * 10, paint);
		}
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		Tocka t = new Tocka();
		t.x = event.getX();
		t.y = event.getY();
		t.pressure = event.getPressure();
		
		tocke.add(t);
		invalidate();
		return true;
		
	}

}


class Tocka {
	float x,y;
	float pressure;
	
}