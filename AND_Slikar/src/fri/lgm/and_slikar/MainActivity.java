package fri.lgm.and_slikar;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.NavUtils;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        DrawView v = new DrawView(this);
        
        setContentView(v);
        v.requestFocus();
    }

   

    
}
