package fri.android;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.hardware.SensorListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;

public class Gyroscope extends Activity {


    private SensorManager mSensorManager;
    private GraphView mGraphView;
    private float   mOrientationValues[] = new float[3];
    
    private class GraphView extends View implements SensorListener{
    	private int view_w, view_h;
        public GraphView(Context context) {
            super(context);
        }
        
        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);
            view_h = h;
            view_w = w;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            synchronized (this) {
//                if (mBitmap != null) {
                    Paint paint = new Paint();
                    paint.setFlags(Paint.ANTI_ALIAS_FLAG);
                    Path path = new Path();
                    RectF rect = new RectF();
                    rect.set(-0.5f, -0.5f, 0.5f, 0.5f);
                    path.arcTo(rect, 0, 180);
                    final int outer = 0xFFC0C0C0;
                    final int inner = 0xFFff7010;


                    float[] values = mOrientationValues;
                    
                    float w0 = view_w * 0.333333f;
                    float w  = w0 - 32;
                    float x = w0*0.5f;
                    for (int i=0 ; i<3 ; i++) {
                        canvas.save(Canvas.MATRIX_SAVE_FLAG);
                        canvas.translate(x, w*0.5f + 4.0f);
                        canvas.save(Canvas.MATRIX_SAVE_FLAG);
                        paint.setColor(outer);
                        canvas.scale(w, w);
                        canvas.drawOval(rect, paint);
                        canvas.restore();
                        canvas.scale(w-5, w-5);
                        paint.setColor(inner);
                        canvas.rotate(-values[i]);
                        canvas.drawPath(path, paint);
                        canvas.restore();
                        x += w0;
                    }

//                }
            }
        }

        public void onSensorChanged(int sensor, float[] values) {
            //Log.d(TAG, "sensor: " + sensor + ", x: " + values[0] + ", y: " + values[1] + ", z: " + values[2]);
            synchronized (this) {
                if (sensor == SensorManager.SENSOR_ORIENTATION) {
                    for (int i=0 ; i<3 ; i++) {
                        mOrientationValues[i] = values[i];
                    }
                }
                invalidate();
            }
        }
        
		public void onAccuracyChanged(int sensor, int accuracy) {
			// TODO Auto-generated method stub
		}
        
        
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mGraphView = new GraphView(this);
        setContentView(mGraphView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mGraphView, 
                SensorManager.SENSOR_ACCELEROMETER | 
                SensorManager.SENSOR_MAGNETIC_FIELD | 
                SensorManager.SENSOR_ORIENTATION,
                SensorManager.SENSOR_DELAY_FASTEST);
    }
    
    @Override
    protected void onStop() {
        mSensorManager.unregisterListener(mGraphView);
        super.onStop();
    }
}