package fri.android;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

public class ProgressBar_ThreadActivity extends Activity {

	ProgressBar myProgressBar;
	int myProgress = 0;
	Handler myHandler;
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		myHandler = new Handler();

		myProgressBar = (ProgressBar) findViewById(R.id.progressbar_Horizontal);

		new Thread(myThread).start();
		
	}

	private Runnable myThread = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			while (myProgress < 100) {
				try {
					//myHandle.sendMessage(myHandle.obtainMessage());
					myProgress++;
					myProgressBar.setProgress(myProgress);
					Thread.sleep(50);
				} catch (Throwable t) {
				}
			}
			myHandler.post(new Runnable() {
			    	public void run() 
			    	{ 
			    		Button izklop = (Button) findViewById(R.id.button1);
						izklop.setOnClickListener(new MojListener());
						izklop.setVisibility(View.VISIBLE);
			    	} 
			    }
			    );
			
			
		}

	};


}