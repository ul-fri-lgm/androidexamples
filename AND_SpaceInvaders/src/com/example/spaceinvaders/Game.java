package com.example.spaceinvaders;

import java.util.Timer;
import java.util.TimerTask;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class Game {
	private GameView gameView;
	private Resources res;
	
	private SpaceShip ship;
	private SpaceShip ship2;
	//private Explosion exp;
	//private Explosion2d exp2d;
	
	private float width = 0, height = 0;
	private boolean shipFocused=false;
	
	private OnTouchListener touchListener = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			int action = event.getAction();
			float xTouch = event.getX();
			float yTouch = event.getY();
			switch (action) {
				case MotionEvent.ACTION_DOWN:
					
					float dx = Math.abs(xTouch - ship.posX);
					float dy = Math.abs(yTouch - ship.posY);
					
					float distance = (float)Math.sqrt( dx*dx + dy*dy );
					shipFocused=false;
					if (distance < ship.getSize()) {
						shipFocused=true;
						Log.d("SpaceInvaders", "Zadel si ladjico.");
					}
					break;
				case MotionEvent.ACTION_MOVE:
					if (shipFocused) {
						ship.posX=xTouch;
						ship.posY=yTouch;
						gameView.postInvalidate();
					}
					break;
				default:
					break;
			}
			
			return true;
		}
	};
	
	public Game(Resources res, GameView gameView, float width, float height) {
		this.gameView = gameView;
		gameView.setOnTouchListener(touchListener);
		this.res = res;
		this.width = width;
		this.height = height;
		init();
	}
	
	private void init() {
		gameView.setGame(this);
		ship = new SpaceShip(res, width, height);
		ship2 = new SpaceShip(res, width, height);
		ship2.scale = 0.5f;
		ship2.posX = 400;
		ship2.posY = 400;
		//exp = new Explosion(res);
		//exp2d = new Explosion2d(res);
		Timer gameTimer = new Timer();
		gameTimer.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				gameLoop();
			}
		}, 0, 40);
	}
	
	private void gameLoop() {
//		Log.d("SpaceInvaders", "OBject: "+ship);
//		Log.d("SpaceInvaders", stevec+": posX: " + ship.posX + " posY:" + ship.posY);
		if (collide(ship, ship2)) {
			Log.d("SpaceInvaders", "Trk!");
		}
		
		gameView.postInvalidate();
	}
	
	private boolean collide(GameObject o1, GameObject o2) {
		float rx = Math.abs(o1.posX - o2.posX);
		float ry = Math.abs(o1.posY - o2.posY);
		if ( Math.sqrt(rx * rx + ry * ry) < (o1.getSize() + o2.getSize()) )
			return true;
		return false;
	}
	
	public void render(Canvas canvas){
		ship.render(canvas);
		ship2.render(canvas);
//		exp.render(canvas);
//		exp2d.render(canvas);
	}
}
