package com.example.spaceinvaders;

import android.graphics.Canvas;

public interface Renderable {
	
	public abstract void render(Canvas canvas);
}
