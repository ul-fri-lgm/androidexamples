package com.example.spaceinvaders;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class GameView extends View{
	Paint mPaint = new Paint();
	Game game;

	public GameView(Context context) {
		super(context);
	}
	
	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public void setGame(Game game) {
		this.game = game;
	}
	
	protected void onDraw(Canvas canvas) {
		if (game != null) { 
			game.render(canvas);
		}
	}

}
