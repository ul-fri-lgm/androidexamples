package com.example.spaceinvaders;

import android.content.res.Resources;
import android.graphics.Paint;

public abstract class GameObject implements Renderable {
	Paint mPaint;
	Resources res;
	float scale = 1.0f;
	float posX  = 0, posY = 0;

	public GameObject(Resources res) {
		mPaint = new Paint();
		this.res = res;
	}
	
	public void setScale(float scale) {
		this.scale = scale;
	}
	
	public abstract float getSize();
}
