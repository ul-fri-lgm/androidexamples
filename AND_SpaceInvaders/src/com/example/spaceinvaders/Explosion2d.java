package com.example.spaceinvaders;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Explosion2d extends GameObject {
	Bitmap image;
	int srcPixelSize = 64;
	int dstPixelSize = 720;
	int wCount = 5;
	int hCount = 5;
	int explosionPosition = 0;
	boolean running = true;

	public Explosion2d(Resources res) {
		super(res);
		image = BitmapFactory.decodeResource(res, R.drawable.explosion2d);
		srcPixelSize = image.getWidth()/wCount;
	}

	@Override
	public void render(Canvas canvas) {
		if (running) {
			int w = explosionPosition % wCount;
			int h = (int)(explosionPosition / hCount);
			Rect src = new Rect(w * srcPixelSize, h * srcPixelSize, (w + 1) * srcPixelSize, (h + 1) * srcPixelSize);
			canvas.drawBitmap(image, src,  new Rect(0, 0, dstPixelSize, dstPixelSize), mPaint);
			explosionPosition = (explosionPosition + 1) % (wCount * hCount);
//			if (explosionPosition == 0) running = false;
		}
		
	}

	@Override
	public float getSize() {
		// TODO Auto-generated method stub
		return 0;
	}

}
