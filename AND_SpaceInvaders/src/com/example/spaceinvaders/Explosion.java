package com.example.spaceinvaders;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;

public class Explosion extends GameObject {
	Bitmap image;
	int srcPixelSize = 128;
	int dstPixelSize = 720;
	int explosionPosition = 0;
	boolean running = true;

	public Explosion(Resources res) {
		super(res);
		image = BitmapFactory.decodeResource(res, R.drawable.explosion);
		srcPixelSize = image.getWidth();
	}

	@Override
	public void render(Canvas canvas) {
		if (running) {
			Rect src = new Rect(explosionPosition * srcPixelSize, 0, (explosionPosition + 1) * srcPixelSize, srcPixelSize);
			canvas.drawBitmap(image, src,  new Rect(0, 0, dstPixelSize, dstPixelSize), mPaint);
			explosionPosition = (explosionPosition + 1) % 5;
//			if (explosionPosition == 0) running = false;
		}
	}

	@Override
	public float getSize() {
		// TODO Auto-generated method stub
		return 0;
	}

}
