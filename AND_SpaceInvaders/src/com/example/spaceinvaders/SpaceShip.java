package com.example.spaceinvaders;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.util.Log;

public class SpaceShip extends GameObject {
	Bitmap image;
	float  width = 0, height = 0;
	
	private final float SHIP_SIZE = 0.1f;

	public SpaceShip(Resources res, float screenWidth, float screenHeight) {
		super(res);
		image = BitmapFactory.decodeResource(this.res, R.drawable.ship);
		width = image.getWidth();
		height = image.getHeight();
		float targetHeight = screenHeight*SHIP_SIZE;
		scale = targetHeight / height;
	}
	
	public float getSize() {
		return scale * height;
	}

	@Override
	public void render(Canvas canvas) {
		float w =  0.5f * scale * width;
		float h = 0.5f * scale * height;
		canvas.drawBitmap(image, null, new RectF(posX - w, posY - h, posX + w, posY + h), mPaint);
	}

}
